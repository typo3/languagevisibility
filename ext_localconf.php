<?php

use TYPO3\CMS\Core\Domain\Repository\PageRepository as CorePageRepository;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Extbase\Persistence\Generic\Storage\Typo3DbBackend as ExtbaseTypo3DbBackend;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer as FrontendContentObjectRenderer;
use TYPO3\CMS\Frontend\ContentObject\Menu\TextMenuContentObject as FrontendTextMenuContentObject;
use TYPO3\CMS\Frontend\ContentObject\RecordsContentObject as FrontendRecordsContentObject;
use TYPO3\Languagevisibility\Hook\TceMainHook;
use TYPO3\Languagevisibility\Updates\VisibilitySettingsAllLanguagesMigrationWizard;
use TYPO3\Languagevisibility\UserFunction\FieldVisibilityUserFunction;
use TYPO3\Languagevisibility\Xclass\ContentObjectRenderer;
use TYPO3\Languagevisibility\Xclass\Gridelements\Gridelements;
use TYPO3\Languagevisibility\Xclass\Mask\InlineHelper;
use TYPO3\Languagevisibility\Xclass\PageRepository;
use TYPO3\Languagevisibility\Xclass\RecordsContentObject;
use TYPO3\Languagevisibility\Xclass\TextMenuContentObject;
use TYPO3\Languagevisibility\Xclass\Typo3DbBackend;

call_user_func(
	static function () {
		// Register TYPO3 core hooks
		$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['processDatamapClass']['languagevisibility'] =
			TceMainHook::class;

		$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['processCmdmapClass']['languagevisibility'] =
			TceMainHook::class;

		$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['moveRecordClass']['languagevisibility'] =
			TceMainHook::class;

		$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['processCmdmap_afterFinish']['languagevisibility'] =
			TceMainHook::class;

		// overriding option because this is done by languagevisibility and will not work if set
		$GLOBALS['TYPO3_CONF_VARS']['FE']['hidePagesIfNotTranslatedByDefault'] = 0;

		// Xclasses
		$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][FrontendTextMenuContentObject::class] =
			['className' => TextMenuContentObject::class];
		$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][ExtbaseTypo3DbBackend::class] =
			['className' => Typo3DbBackend::class];
		$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][FrontendRecordsContentObject::class] =
			['className' => RecordsContentObject::class];
		$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][FrontendContentObjectRenderer::class] =
			['className' => ContentObjectRenderer::class];
		$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][CorePageRepository::class] =
			['className' => PageRepository::class];

		if (ExtensionManagementUtility::isLoaded('mask')) {
			$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\MASK\Mask\Helper\InlineHelper::class] =
				['className' => InlineHelper::class];
		}

		// Node registration
		$GLOBALS['TYPO3_CONF_VARS']['SYS']['formEngine']['nodeRegistry'][1597679991] = [
			'nodeName' => 'languageVisibility',
			'priority' => 40,
			'class' => FieldVisibilityUserFunction::class,
		];
	}
);
