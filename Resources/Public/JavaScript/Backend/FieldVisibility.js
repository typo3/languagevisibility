class FieldVisibility {
	init() {
		const applyRecursiveCheckboxes = document.querySelectorAll('.apply-recursive');
		for (const applyRecursiveCheckbox of applyRecursiveCheckboxes) {
			applyRecursiveCheckbox.addEventListener('click', (event) => {
				if (event.currentTarget.checked) {
					alert(event.currentTarget.dataset.alert);
				}
			});
		}

		const enableLoggingCheckboxes = document.querySelectorAll('.enable-logging');
		for (const enableLoggingCheckbox of enableLoggingCheckboxes) {
			enableLoggingCheckbox.addEventListener(
				'click',
				(event) => this.togglehiddenEnableLoggingInputs(
					event.currentTarget.dataset.classname
				)
			);
		}
	}

	togglehiddenEnableLoggingInputs(className) {
		const inputs = document.querySelectorAll('.' + className);
		for (const input of inputs) {
			if (input.value === 1) {
				input.value = 0;
			} else {
				input.value = 1;
			}
		}
	}
}

export default new FieldVisibility();
