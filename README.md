# EXT: languagevisibility

<img src="https://www.sgalinski.de/typo3conf/ext/project_theme/Resources/Public/Images/logo.svg" />

License: [GNU GPL, Version 2](https://www.gnu.org/licenses/gpl-2.0.html)

Repository: https://gitlab.sgalinski.de/typo3/languagevisibility

## Configuration

Use this TypoScript code to set up your language behavior in the FE:

```TypoScript
config.sys_language_mode = ignore
config.sys_language_overlay = 1

//normal language configuration:
config.sys_language_uid = 0
config.language = en
config.htmlTag_langKey = en
config.locale_all = en_GB.utf8

//deutsch
[globalVar = GP:L=1]
    config.sys_language_uid = 1
    config.language = de
    config.htmlTag_langKey = de
    config.locale_all = de_DE.utf8
[global]
...
```

## Use languagevisibility for own records

There are 3 steps to take:

1. Extend your table with the required field and TCA
2. Register your table to the languagevisibility core
3. Use the correct code so that the desired functionality happened

### 1. Extend your table

Add this definition to your table TCA configuration:

```PHP
'tx_languagevisibility_visibility' => [
	'exclude' => 1,
	'label' => 'LLL:EXT:languagevisibility/locallang_db.xlf:pages.tx_languagevisibility_visibility',
	'config' => [
		'type' => 'user',
        'renderType' => 'languageVisibility'
	]
];
```

And to ext_tables.sql add:

```SQL
tx_languagevisibility_visibility text
```

### 2. Register your table

Use the existing registration Hook :

```PHP
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['languagevisibility']['recordElementSupportedTables'][<table>]= [];
```

(this will handle your table like a default record element. If you need more control you can also register your own
visibility element class)

## Note

If you want to access the data directly with the Doctrine Querybuilder, you need to add the following code manually,
if the languagevisibility matters:

```PHP
if (FrontendServices::isSupportedTable($tableName)) {
    if (FrontendServices::checkVisiblityForElement($row, $tableName, $languageUid)) {
        return;
    }
}
```
