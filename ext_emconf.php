<?php

########################################################################
# Extension Manager/Repository config file for ext "languagevisibility".
#
# Manual updates:
# Only the data in the array - everything else is removed by next
# writing. "version" and "dependencies" must not be touched!
########################################################################

$EM_CONF['languagevisibility'] = [
	'title' => 'Language Visibility',
	'description' => 'Language Visibility',
	'category' => 'fe',
	'author' => 'Fabian Galinski',
	'author_company' => 'sgalinski Internet Services (https://www.sgalinski.de)',
	'author_email' => 'fabian@sgalinski.de',
	'state' => 'stable',
	'version' => '5.0.14',
	'constraints' => [
		'depends' => [
			'php' => '8.1.0-8.3.99',
			'typo3' => '12.4.0 - 12.4.99',
		],
		'conflicts' => [
		],
		'suggests' => [
		],
	],
];
