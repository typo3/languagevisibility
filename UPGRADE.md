# Version 5.x

- Dropped Version 10 and 11 Support
- Dropped VisibilitySettingsMigrationWizard
- Removed Tca.php class because it was unused
- Removed Gridelements.php xClass
- Moved isSupportedTable static function to VisibilityService
- Changed FieldVisibility.js from requireJS to JavaScript module

# Version 4.x

- Dropped TYPO3 V9 Support

# From Version 2.X to 3.X

- Perform a database backup.
- Install Tool: Add the new table "tx_languagevisibility_visibility_flag".
- Install Tool: Apply all changed fields.
- Upgrade Wizard: Execute the "VisibilitySettingsMigrationWizard"-Wizard.
- Remove all old fields.

## Additional Breaking Changes for Version 3.1

- Dropped TYPO3 8 Support

### Migrate the "Extend your table" registrations to the new code

The function "user_fieldvisibility" is deprecated and might be removed in the next major version.

**Old Code:**

```php
'tx_languagevisibility_visibility' => [
    'exclude' => 1,
    'label' => 'LLL:EXT:languagevisibility/locallang_db.xlf:pages.tx_languagevisibility_visibility',
    'config' => [
        'type' => 'user',
        'userFunc' => 'TYPO3\Languagevisibility\UserFunction\FieldVisibilityUserFunction->user_fieldvisibility',
    ]
],
```

**New Code:**

```php
'tx_languagevisibility_visibility' => [
	'exclude' => 1,
	'label' => 'LLL:EXT:languagevisibility/locallang_db.xlf:pages.tx_languagevisibility_visibility',
	'config' => [
		'type' => 'user',
        'renderType' => 'languageVisibility'
	]
]
```
