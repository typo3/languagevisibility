CREATE TABLE pages (
	tx_languagevisibility_visibility text
);

CREATE TABLE tt_content (
	tx_languagevisibility_visibility text
);

CREATE TABLE tx_languagevisibility_visibility_flag (
	record_table        text,
	record_uid          text,
	record_language_uid int(11) DEFAULT '0' NOT NULL,
	flag                text
);
