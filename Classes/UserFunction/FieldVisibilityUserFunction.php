<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace TYPO3\Languagevisibility\UserFunction;

use Exception;
use TYPO3\CMS\Backend\Form\Element\AbstractFormElement;
use TYPO3\CMS\Backend\Form\NodeFactory;
use TYPO3\CMS\Core\Imaging\Icon;
use TYPO3\CMS\Core\Imaging\IconFactory;
use TYPO3\CMS\Core\Page\JavaScriptModuleInstruction;
use TYPO3\CMS\Core\Site\Entity\SiteLanguage;
use TYPO3\CMS\Core\Site\SiteFinder;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\MathUtility;
use TYPO3\CMS\Core\Utility\StringUtility;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use TYPO3\Languagevisibility\Element\Element;
use TYPO3\Languagevisibility\Element\ElementFactory;
use TYPO3\Languagevisibility\Repository\VisibilityFlagRepository;
use TYPO3\Languagevisibility\Service\BackendServices;
use TYPO3\Languagevisibility\Service\VisibilityService;

/**
 * User Functions for the use inside the TCA
 */
class FieldVisibilityUserFunction extends AbstractFormElement {
	/**
	 * @var bool $isNewElement
	 */
	private bool $isNewElement = FALSE;

	/**
	 * @var ElementFactory
	 */
	protected ElementFactory $elementFactory;

	/**
	 * @var SiteFinder
	 */
	protected SiteFinder $siteFinder;

	/**
	 * @var VisibilityFlagRepository
	 */
	protected VisibilityFlagRepository $visibilityFlagRepository;

	/**
	 * @var VisibilityService
	 */
	protected VisibilityService $visibilityService;

	/**
	 * This property is meant to persist after the original iconFactory property is removed with v13
	 *
	 * @var IconFactory
	 */
	protected IconFactory $iconFactory2;

	/**
	 * @var BackendServices
	 */
	protected BackendServices $backendServices;

	/**
	 * @param NodeFactory|NULL $nodeFactory
	 * @param array $data
	 * @todo Change to a Dependency Injection constructor in TYPO3 13
	 */
	public function __construct(NodeFactory $nodeFactory = NULL, array $data = []) {
		parent::__construct($nodeFactory, $data);
		$this->visibilityService = GeneralUtility::makeInstance(VisibilityService::class);
		$this->visibilityFlagRepository = GeneralUtility::makeInstance(VisibilityFlagRepository::class);
		$this->backendServices = GeneralUtility::makeInstance(BackendServices::class);
		/**
		 * @deprecated @TODO Rename to iconFactory when the original property gets removed in TYPO3 v13
		 */
		$this->iconFactory2 = GeneralUtility::makeInstance(IconFactory::class);
		$this->siteFinder = GeneralUtility::makeInstance(SiteFinder::class);
		$this->elementFactory = GeneralUtility::makeInstance(ElementFactory::class);
	}

	/**
	 * @return array
	 * @throws \Doctrine\DBAL\Driver\Exception
	 */
	public function render(): array {
		$result = $this->initializeResultArray();
		$content = '';

		// init some class attributes
		$pageId = $this->data['effectivePid'];
		$uid = $this->data['databaseRow']['uid'];
		$row = $this->data['databaseRow'];

		// no language visibility table for root or unsaved pages
		if (
			(MathUtility::canBeInterpretedAsInteger($row['uid']) && $row['uid'] <= 0)
			|| str_starts_with($row['uid'], 'NEW')
		) {
			return ['html' =>
				'<div class="alert alert-info">' .
				$this->getLLL('backend.error.noVisibilitySettingsAvailable') .
				'</div>'
			];
		}

		// use the effectivePid as the database row pid instead of the "sorting" pid
		$row['pid'] = $pageId;

		if (str_starts_with($uid, 'NEW')) {
			$this->isNewElement = TRUE;
		}

		$table = $this->data['tableName'];
		if ($table === 'pages' && !$this->isNewElement) {
			$pageId = $row['uid'];
		}

		$changeable = $this->isEditable($table);

		if (isset($row[$GLOBALS['TCA'][$table]['ctrl']['languageField']][0])) {
			$row[$GLOBALS['TCA'][$table]['ctrl']['languageField']] =
				(int) $row[$GLOBALS['TCA'][$table]['ctrl']['languageField']][0];
		}
		if (isset($row[$GLOBALS['TCA'][$table]['ctrl']['transOrigPointerField']][0])) {
			$row[$GLOBALS['TCA'][$table]['ctrl']['transOrigPointerField']] =
				(int) ($row[$GLOBALS['TCA'][$table]['ctrl']['transOrigPointerField']][0]['uid'] ?? $row[$GLOBALS['TCA'][$table]['ctrl']['transOrigPointerField']][0]);
		}

		try {
			// This element is an original element (no overlay)
			$originalElement = $this->elementFactory->getElementForTable($table, $row);
		} catch (Exception $exception) {
			$result['html'] = '<div class="alert alert-danger">' .
				$this->getLLL('backend.error.elementNotSupported') . ' (' . $exception->getMessage() . ')</div>';
			return $result;
		}

		if ($originalElement->isMonolithicTranslated()) {
			$result['html'] = $content;
			return $result;
		}

		try {
			$site = $this->siteFinder->getSiteByPageId($pageId);
		} catch (Exception $e) {
			$result['html'] = '<div class="alert alert-danger">' .
				$this->getLLL('backend.error.siteNotFound') . '</div>';
			return $result;
		}

		$languageList = $site->getLanguages();
		$infosStruct = $this->_getLanguageInfoStructurListForElementAndLanguageList(
			$originalElement,
			$languageList,
			$this->data['parameterArray']['itemFormElName'],
			FALSE
		);

		if (count($infosStruct) <= 0) {
			$result['html'] = '';
			return $result;
		}

		$legend = '<div><br />'
			. LocalizationUtility::translate('backend.visibilityLegend', 'languagevisibility') .
			'</div>';

		$content .= $this->renderLanguageInfos($infosStruct, ($table === 'pages'), $changeable);

		if ($table === 'pages' && $changeable) {
			$result['javaScriptModules'] = [
				JavaScriptModuleInstruction::create('@sgalinski/languagevisibility/backend/FieldVisibility.js')
					->invoke('init')
			];
			$nameBase = 'tx_languagevisibility_visibility_flag';
			$hiddenEnableLoggingClass = 'contentElement-' . $uid . '-hiddenEnableLoggingInput';
			$enableLoggingId = $nameBase . '-enableLogging';
			$content .= '<div class="checkbox checkbox-type-icon-toggle">
				<input type="checkbox"
					class="checkbox-input"
					value="1"
					checked="checked"
					data-classname="' . $hiddenEnableLoggingClass . '"
					name="master[enableLogging]"
					id="' . $enableLoggingId . '" />
				<label class="checkbox-label" for="' . $nameBase . '-enableLogging" >
					<span class="checkbox-label-text">' . $this->getLLL('enableLogging') . '</span>
				</label>
			</div>';
		}

		$result['html'] = '<div id="fieldvisibility">' . $content . $legend . '</div>';
		return $result;
	}

	/**
	 * Checks if the given table is editable by the logged in backend user
	 *
	 * @param string $table
	 * @return bool
	 */
	protected function isEditable(string $table): bool {
		return $GLOBALS['BE_USER']->check('tables_modify', $table) &&
			$GLOBALS['BE_USER']->check('tables_modify', 'tx_languagevisibility_visibility_flag') &&
			$GLOBALS['BE_USER']->check('non_exclude_fields', $table . ':tx_languagevisibility_visibility');
	}

	/**
	 * This method is used to generate an infoStructure array, which will be
	 * rendered as a Form
	 *
	 * @param Element $changeableElement
	 * @param array $languageList
	 * @param string $itemFormElName
	 * @param boolean $isOverlay
	 * @return array
	 * @throws \Doctrine\DBAL\Exception
	 */
	protected function _getLanguageInfoStructurListForElementAndLanguageList(
		Element $changeableElement,
		array $languageList,
		string $itemFormElName,
		bool $isOverlay
	): array {
		$this->visibilityFlagRepository->flushFlagCache();
		$infosStruct = [];
		$changeableElementTable = $changeableElement->getTable();
		$uid = $changeableElement->getUid();
		if ($changeableElement->isNew()) {
			$uid = $changeableElement->getNewUid();
		}

		if ($changeableElement->getTable() === 'pages') {
			$pid = $uid;
		} else {
			$pid = $changeableElement->getPid();
		}

		$recordUid = $changeableElementTable . '_' . $uid;

		/** @var SiteLanguage $language */
		foreach ($languageList as $language) {
			$languageId = $language->getLanguageId();
			if ($languageId < 0) {
				continue;
			}

			$infoitem = [
				'languageTitle' => $language->getTitle(),
				'languageFlag' => $this->iconFactory2->getIcon(
					$language->getFlagIdentifier(),
					Icon::SIZE_SMALL
				)->render(),
				'languageUid' => $language->getLanguageId(),
				'hasTranslation' => $changeableElement->hasTranslation($languageId),
				'isTranslation' => $isOverlay,
				'isVisible' => $this->visibilityService->isVisible($language, $changeableElement),
				'visibilityDescription' => $this->visibilityService->getVisibilityDescription(
					$language,
					$changeableElement
				)
			];

			// this is to not be able as a translator to override languageSetting
			$currentOptionsForUserAndLanguage = $this->backendServices->getAvailableOptionsForLanguage(
				$language,
				$isOverlay
			);

			// check if a flag for this localization/overlay has already been set
			$visbilityFlag = $this->visibilityFlagRepository->getVisibilityFlag(
				$changeableElementTable,
				$uid,
				$languageId
			);

			if (!is_null($visbilityFlag)) {
				$visibilityFlagUid = $visbilityFlag['uid'];
				$currentVisibilityFlag = $visbilityFlag['flag'];
			} else {
				$visibilityFlagUid = StringUtility::getUniqueId('NEW');
				$currentVisibilityFlag = 'active';
			}

			//build array with names and values for hidden input
			$nameBase = 'data[tx_languagevisibility_visibility_flag][' . $visibilityFlagUid . ']';
			$nameArray = [
				'record_language_uid' => [
					'name' => $nameBase . '[record_language_uid]',
					'value' => $languageId
				],
				'record_table' => [
					'name' => $nameBase . '[record_table]',
					'value' => $changeableElementTable
				],
				'record_uid' => [
					'name' => $nameBase . '[record_uid]',
					'value' => $recordUid
				],
				'flag' => [
					'name' => $nameBase . '[flag]',
					'value' => $currentVisibilityFlag,
				],
				'pid' => [
					'name' => $nameBase . '[pid]',
					'value' => $pid,
				],
			];

			$visibilityString = $currentOptionsForUserAndLanguage[$currentVisibilityFlag] ?? '';
			$selectBox = $this->getSelectBoxAndHiddenInputs(
				$languageId,
				$currentOptionsForUserAndLanguage,
				$currentVisibilityFlag,
				$nameArray
			);

			$infoitem['visibilityOptions'] = $selectBox;
			$infoitem['visibilityCurrently'] = $visibilityString;

			$alertText = 'Be careful, when using the recursive function. This can destroy the whole frontend!';
			$recursiveId = $nameBase . '-applyRecursive-' . $languageId;
			$infoitem['applyRecursiveCheckbox'] = '<div class="checkbox checkbox-type-icon-toggle">
					<input type="checkbox"
						class="checkbox-input apply-recursive"
						value="1"
						data-alert="' . $alertText . '"
						name="' . $nameBase . '[applyRecursive]"
						id="' . $recursiveId . '" />
				</div>';

			// the enable logging info is required for each language now, therefore a hidden field is added for each
			// language which is changed by the main enableLogging Checkbox
			$hiddenEnableLoggingClass = 'contentElement-' . $uid . '-hiddenEnableLoggingInput';
			$infoitem['hiddenEnableLoggingInput'] = '<input type="hidden"
					value="1"
					name="' . $nameBase . '[enableLogging]"
					 class="' . $hiddenEnableLoggingClass . '" />';
			$infosStruct[] = $infoitem;
		}

		return $infosStruct;
	}

	/**
	 * Generates the select box and hidden inputs for the language visibility flag of an item and its translations
	 *
	 * @param int $languageid
	 * @param array $select
	 * @param string $current current selected item
	 * @param array $nameArray
	 * @return string
	 */
	protected function getSelectBoxAndHiddenInputs(
		int $languageid,
		array $select,
		string $current,
		array $nameArray
	): string {
		$content = '';
		$addClassName = '';
		if (count($select) === 1) {
			$addClassName = ' oneitem';
		}

		$content .= '<input type="hidden" name="' . $nameArray['record_table']['name'] . '" value="' . $nameArray['record_table']['value'] . '" />';
		$content .= '<input type="hidden" name="' . $nameArray['record_uid']['name'] . '" value="' . $nameArray['record_uid']['value'] . '" />';
		$content .= '<input type="hidden" name="' . $nameArray['record_language_uid']['name'] . '" value="' . $nameArray['record_language_uid']['value'] . '" />';
		$content .= '<input type="hidden" name="' . $nameArray['pid']['name'] . '" value="' . $nameArray['pid']['value'] . '" />';

		$content .= '<select class="form-control' . $addClassName . '" name="' . $nameArray['flag']['name'] . '">';
		foreach ($select as $skey => $svalue) {
			$selected = '';
			if ($current === $skey) {
				$selected = 'selected="selected"';
			}
			$content .= '<option class="' . $this->getCSSClassFromVisibilityKey($skey) .
				'" value="' . $skey . '" ' . $selected . '>' . $svalue . '</option>';
		}
		$content .= '</select>';
		return $content;
	}

	/**
	 * This method is used to determine a css class for the different visibility modes
	 *
	 * @param string $key
	 * @return string
	 */
	protected function getCSSClassFromVisibilityKey(string $key): string {
		$res = '';
		switch ($key) {
			case 'inactive':
			case 'translated':
				$res = $key;
				break;
		}

		return $res;
	}

	/**
	 * Returns the HTML for the generated languagevisibility module.
	 *
	 * @param array $infosStruct
	 * @param boolean $isPage
	 * @param bool $visibilityChangeable
	 * @return string
	 */
	protected function renderLanguageInfos(array $infosStruct, bool $isPage, bool $visibilityChangeable): string {
		$content = '<table class="table table-striped table-hover">';
		$content .= '<tr>' .
			'<th>' . $this->getLLL('language') . '</th>' .
			'<th>' . $this->getLLL('visibility_options') . '</th>' .
			'<th>' . $this->getLLL('visibility_currently') . '</th>' .
			'<th>' . $this->getLLL('hastranslation') . '</th>' .
			'<th>' . $this->getLLL('isshown') . '</th>' .
			($isPage ? '<th>' . $this->getLLL('applyRecursive') . '</th>' : '') .
			'</tr>';

		foreach ($infosStruct as $info) {
			if ($info['hasTranslation']) {
				$translationStatusIcon =
					$this->iconFactory2->getIcon('actions-check', Icon::SIZE_SMALL)
						->render('inline');
			} else {
				$translationStatusIcon =
					$this->iconFactory2->getIcon('empty-empty', Icon::SIZE_SMALL)
						->render('inline');
			}

			if ($info['isVisible']) {
				$visibilityStatusIcon =
					$this->iconFactory2->getIcon('actions-check', Icon::SIZE_SMALL)
						->render('inline');
			} else {
				$visibilityStatusIcon =
					$this->iconFactory2->getIcon('empty-empty', Icon::SIZE_SMALL)
						->render('inline');
			}

			$isChangeable = $visibilityChangeable;
			if ($isChangeable && !$GLOBALS['BE_USER']->checkLanguageAccess($info['languageUid'])) {
				// if the particular language can not be edited by the user, switch changeable to false
				$isChangeable = FALSE;
			}

			$content .= '<tr>' .
				'<td>' . $info['languageFlag'] . ' ' . $info['languageTitle'] . '</td>' .
				(!$isChangeable ? '<td></td>' : '<td>' . $info['visibilityOptions'] . '</td>') .
				'<td>' . $info['visibilityCurrently'] . '</td>' .
				'<td>' . $translationStatusIcon . '</td>' .
				'<td>' . $visibilityStatusIcon . '</td>' .
				($isPage && $isChangeable ? '<td>' . $info['applyRecursiveCheckbox'] . ' ' . $info['hiddenEnableLoggingInput'] . ($info['isVisibilityFlag'] ?? '') . '</td>' : '') .
				'</tr>';
		}

		$content .= '</table>';
		return $content;
	}

	/**
	 * @param string $key
	 * @return string
	 */
	public function getLLL(string $key): string {
		return LocalizationUtility::translate(
			'LLL:EXT:languagevisibility/Resources/Private/Language/locallang_db.xlf:' . $key,
			'languagevisibility'
		);
	}
}
