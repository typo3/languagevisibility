<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace TYPO3\Languagevisibility\Updates;

use Doctrine\DBAL\Exception;
use TYPO3\CMS\Core\Authentication\BackendUserAuthentication;
use TYPO3\CMS\Core\Authentication\CommandLineUserAuthentication;
use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\Restriction\DeletedRestriction;
use TYPO3\CMS\Core\Localization\LanguageServiceFactory;
use TYPO3\CMS\Core\Site\SiteFinder;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Install\Attribute\UpgradeWizard;
use TYPO3\CMS\Install\Updates\UpgradeWizardInterface;
use TYPO3\Languagevisibility\Repository\VisibilityFlagRepository;
use TYPO3\Languagevisibility\Service\VisibilityService;

/**
 * Class VisibilitySettingsAllLanguagesMigrationWizard
 *
 * @package TYPO3\Languagevisibility\Updates
 */
#[UpgradeWizard('VisibilitySettingsAllLanguagesMigrationWizard')]
class VisibilitySettingsAllLanguagesMigrationWizard implements UpgradeWizardInterface {
	/**
	 * @var VisibilityFlagRepository
	 */
	protected VisibilityFlagRepository $visibilityFlagRepository;

	/**
	 * @var VisibilityService
	 */
	protected VisibilityService $visibilityService;

	/**
	 * @var ConnectionPool
	 */
	protected ConnectionPool $connectionPool;

	/**
	 * mapping of old to new visibility flag strings
	 *
	 * @var string[]
	 */
	protected array $oldToNewFlagMapping = [
		'-' => 'active',
		'yes' => 'enforce',
		'yes+' => 'enforce',
		't' => 'translated',
		'f' => 'fallback',
		'no' => 'inactive',
		'no+' => 'inactive',
	];

	/**
	 * Return the speaking name of this wizard
	 *
	 * @return string
	 */
	public function getTitle(): string {
		return 'Migrate visibility-settings from record-based json config to the relational table for the missing all languages records';
	}

	/**
	 * Return the description for this wizard
	 *
	 * @return string
	 */
	public function getDescription(): string {
		return 'This wizard iterates through all tables that are configured to work with language visibility and a language setting all (-1) and migrates the existing settings saved in the table rows to the relational settings table';
	}

	/**
	 * Execute the update
	 *
	 * Called when a wizard reports that an update is necessary
	 *
	 * @return bool
	 * @throws Exception
	 */
	public function executeUpdate(): bool {
		$this->initializeBackendUserAuthentication();
		$this->visibilityFlagRepository = GeneralUtility::makeInstance(VisibilityFlagRepository::class);
		$this->visibilityService = GeneralUtility::makeInstance(VisibilityService::class);
		$this->connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
		$siteFinder = GeneralUtility::makeInstance(SiteFinder::class);
		$sites = $siteFinder->getAllSites();

		$availableLanguages = [];

		// make sure we get ALL the languages
		foreach ($sites as $site) {
			$availableLanguagesFromSites = $site->getAllLanguages();
			foreach ($availableLanguagesFromSites as $availableLanguage) {
				if (!isset($availableLanguages[$availableLanguage->getLanguageId()])) {
					$availableLanguages[$availableLanguage->getLanguageId()] = $availableLanguage;
				}
			}
		}

		unset($sites);

		// go through all configured tables
		foreach ($GLOBALS['TCA'] as $table => $config) {
			// check if table is supported
			if (in_array(
				$table,
				$this->visibilityService->getSupportedTables(),
				TRUE
			)) {
				$uids = $this->getUidsWithLanguageAll($table);

				foreach ($uids as $uidRow) {
					$uid = $uidRow['uid'];

					if ($uid > 0) {
						$row = $this->getRecord($table, $uid);
						$defaultLanguageRecordUid = $uid;

						$localVisibilitySettings = @unserialize(
							$row['tx_languagevisibility_visibility'],
							['allowed_classes' => FALSE]
						);

						// skip possible manually edited Records.
						$existingFlags = $this->getExistingFlags($table, $uid);
						if ($existingFlags) {
							continue;
						}
						foreach ($availableLanguages as $language) {
							$lid = $language->getLanguageId();

							$queryBuilderVisibilityFlags = $this->connectionPool->getQueryBuilderForTable(
								'tx_languagevisibility_visibility_flag'
							);

							$visibilitySetting = $localVisibilitySettings[$lid] ?? NULL;
							if ($visibilitySetting) {
								// the pid should always be the default language page
								if ($table === 'pages') {
									if ($lid > 0) {
										$pid = $defaultLanguageRecordUid;
									} else {
										$pid = $uid;
									}
								} else {
									$pid = $row['pid'];
								}

								// the record_uid should always be the default language record
								if ($lid > 0) {
									$recordUid = $table . '_' . $defaultLanguageRecordUid;
								} else {
									$recordUid = $table . '_' . $uid;
								}

								$newVisibilityFlag = $this->oldToNewFlagMapping[$visibilitySetting];

								$queryBuilderVisibilityFlags
									->insert('tx_languagevisibility_visibility_flag')
									->values(
										[
											'pid' => $pid,
											'flag' => $newVisibilityFlag,
											'record_table' => $table,
											'record_uid' => $recordUid,
											'record_language_uid' => $lid,
											'tstamp' => time(),
											'crdate' => time(),
										]
									)
									->executeQuery();
							}
						}
					}
				}
			}
		}

		// cleanup
		$this->visibilityFlagRepository->deleteDefaultFlags();

		return TRUE;
	}

	/**
	 * returns only Records with sys_language_uid = -1
	 *
	 * @param $table
	 * @return array
	 * @throws Exception
	 */
	protected function getUidsWithLanguageAll($table): array {
		$queryBuilder = $this->connectionPool->getQueryBuilderForTable(
			$table
		);

		if (array_key_exists('enable_colums', $GLOBALS['TCA'][$table]['ctrl'])) {
			//remove restrictions so that hidden records also get migrated
			$queryBuilder
				->getRestrictions()
				->removeAll()
				->add(GeneralUtility::makeInstance(DeletedRestriction::class));

			// get all default_language_record uids
			$uids = $queryBuilder->select('uid')
				->from($table)
				->where(
					$queryBuilder->expr()->eq(
						'sys_language_uid',
						$queryBuilder->createNamedParameter(-1, \PDO::PARAM_INT)
					)
				)
				->executeQuery()->fetchAllAssociative();
		} else {
			// falls back for tables with dynamically generated enable columns

			//remove restrictions so that hidden records also get migrated
			$queryBuilder
				->getRestrictions()
				->removeAll();

			// get all default_language_record uids
			$uids = $queryBuilder->select('uid')
				->from($table)
				->where(
					$queryBuilder->expr()->eq(
						'sys_language_uid',
						$queryBuilder->createNamedParameter(-1, \PDO::PARAM_INT)
					)
				)
				->andWhere(
					$queryBuilder->expr()->eq(
						'deleted',
						$queryBuilder->createNamedParameter(0, \PDO::PARAM_INT)
					)
				)
				->executeQuery()->fetchAllAssociative();
		}

		return $uids;
	}

	/**
	 * Fetches a record from table for given uid
	 *
	 * @param $table
	 * @param $uid
	 * @return array
	 * @throws Exception
	 */
	protected function getRecord($table, $uid): array {
		$queryBuilder = $this->connectionPool->getQueryBuilderForTable(
			$table
		);

		if (array_key_exists('enable_colums', $GLOBALS['TCA'][$table]['ctrl'])) {
			//remove restrictions so that hidden records also get migrated
			$queryBuilder
				->getRestrictions()
				->removeAll()
				->add(GeneralUtility::makeInstance(DeletedRestriction::class));

			return $queryBuilder->select('*')
				->from($table)
				->where(
					$queryBuilder->expr()->eq('uid', $queryBuilder->createNamedParameter($uid))
				)
				->setMaxResults(1)
				->executeQuery()->fetchAllAssociative();
		}

		// falls back to the handler for tables with dynamically generated enable columns

		// remove restrictions so that hidden records also get migrated
		$queryBuilder
			->getRestrictions()
			->removeAll();

		return $queryBuilder->select('*')
			->from($table)
			->where(
				$queryBuilder->expr()->eq('uid', $queryBuilder->createNamedParameter($uid))
			)
			->andWhere(
				$queryBuilder->expr()->eq(
					'deleted',
					$queryBuilder->createNamedParameter(0, \PDO::PARAM_INT)
				)
			)
			->setMaxResults(1)
			->executeQuery()->fetchAllAssociative();
	}

	/**
	 * Is an update necessary?
	 *
	 * Is used to determine whether a wizard needs to be run.
	 * Check if data for migration exists.
	 *
	 * @return bool
	 */
	public function updateNecessary(): bool {
		return TRUE;
	}

	/**
	 * Returns an array of class names of prerequisite classes
	 *
	 * This way a wizard can define dependencies like "database up-to-date" or
	 * "reference index updated"
	 *
	 * @return string[]
	 */
	public function getPrerequisites(): array {
		return [];
	}

	/**
	 * Initialize a BackendUserAuthentication if none exists
	 */
	protected function initializeBackendUserAuthentication(): void {
		if (!$GLOBALS['BE_USER'] instanceof BackendUserAuthentication) {
			if (Environment::isCli()) {
				$GLOBALS['BE_USER'] = GeneralUtility::makeInstance(CommandLineUserAuthentication::class);
			} else {
				$GLOBALS['BE_USER'] = GeneralUtility::makeInstance(BackendUserAuthentication::class);
				$GLOBALS['BE_USER']->start($GLOBALS['TYPO3_REQUEST']);
			}

			$GLOBALS['LANG'] = GeneralUtility::makeInstance(LanguageServiceFactory::class)->createFromUserPreferences($GLOBALS['BE_USER']);
		}
	}

	/**
	 * checks for existing Flags for a Record
	 *
	 * @param $table
	 * @param $uid
	 * @return array
	 * @throws Exception
	 */
	private function getExistingFlags($table, $uid): array {
		$queryBuilderVisibilityFlags = $this->connectionPool->getQueryBuilderForTable(
			'tx_languagevisibility_visibility_flag'
		);
		$recordUid = $table . '_' . $uid;
		return $queryBuilderVisibilityFlags->select('record_uid')
			->from('tx_languagevisibility_visibility_flag')
			->where(
				$queryBuilderVisibilityFlags->expr()->eq(
					'record_table',
					$queryBuilderVisibilityFlags->createNamedParameter($table)
				),
				$queryBuilderVisibilityFlags->expr()->eq(
					'record_uid',
					$queryBuilderVisibilityFlags->createNamedParameter($recordUid)
				)
			)
			->executeQuery()
			->fetchAllAssociative();
	}
}
