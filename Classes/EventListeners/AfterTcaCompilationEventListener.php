<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace TYPO3\Languagevisibility\EventListeners;

use TYPO3\CMS\Core\Configuration\Event\AfterTcaCompilationEvent;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

/**
 * Integration Helper for adding the Language Visibility into the TCA
 */
class AfterTcaCompilationEventListener {
	/**
	 * Alter all types of the pages and tt_content table to include the languagevisibility field
	 * Still needed with new version, since the languagevisibility field should still be displayed in all pages/tt_content types
	 */
	public function __invoke(AfterTcaCompilationEvent $event) {
		$tca = $event->getTca();

		$typeList = ['withoutLanguageTab' => [], 'withLanguageTab' => []];
		foreach ($tca['pages']['types'] as $typeNumber => $value) {
			if (!str_contains($value['showitem'], 'locallang_tabs.xlf:language')) {
				$typeList['withoutLanguageTab'][] = $typeNumber;
			} else {
				$typeList['withLanguageTab'][] = $typeNumber;
			}
		}

		ExtensionManagementUtility::addToAllTCAtypes(
			'pages',
			'tx_languagevisibility_visibility;;;;1-1-1',
			implode(',', $typeList['withLanguageTab']),
			'before:l18n_cfg'
		);

		$tabName = '--div--;LLL:EXT:languagevisibility/Resources/Private/Language/locallang_db.xlf:tabname,';
		ExtensionManagementUtility::addToAllTCAtypes(
			'pages',
			$tabName . 'tx_languagevisibility_visibility;;;;1-1-1',
			implode(',', $typeList['withoutLanguageTab'])
		);

		### content

		$typeList = ['withoutLanguageTab' => [], 'withLanguageTab' => []];
		foreach ($tca['tt_content']['types'] as $typeNumber => $value) {
			$languageConfigItem = $value['showitem'] ?? '';
			if (!str_contains($languageConfigItem, 'locallang_tabs.xlf:language')) {
				$typeList['withoutLanguageTab'][] = $typeNumber;
			} else {
				$typeList['withLanguageTab'][] = $typeNumber;
			}
		}

		ExtensionManagementUtility::addToAllTCAtypes(
			'tt_content',
			'tx_languagevisibility_visibility,',
			implode(',', $typeList['withLanguageTab']),
			'before:sys_language_uid'
		);

		ExtensionManagementUtility::addToAllTCAtypes(
			'tt_content',
			$tabName . 'tx_languagevisibility_visibility,',
			implode(',', $typeList['withoutLanguageTab'])
		);

		$event->setTca($GLOBALS['TCA']);
	}
}
