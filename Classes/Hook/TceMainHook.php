<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace TYPO3\Languagevisibility\Hook;

use Exception;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\Restriction\DeletedRestriction;
use TYPO3\CMS\Core\DataHandling\DataHandler;
use TYPO3\CMS\Core\Site\SiteFinder;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\MathUtility;
use TYPO3\CMS\Core\Utility\StringUtility;
use TYPO3\Languagevisibility\Repository\VisibilityFlagRepository;
use TYPO3\Languagevisibility\Service\VisibilityService;

/**
 * Class TceMainHook
 */
class TceMainHook {
	/**
	 * Holds the current ErrorLog
	 *
	 * @var array
	 */
	public array $errorLog = [];

	/**
	 * @var VisibilityService
	 */
	protected VisibilityService $visibilityService;

	/**
	 * @var VisibilityFlagRepository
	 */
	protected VisibilityFlagRepository $visibilityFlagRepository;

	/**
	 * @var SiteFinder
	 */
	protected SiteFinder $siteFinder;

	/**
	 * @var ConnectionPool
	 */
	protected ConnectionPool $connectionPool;

	public function __construct() {
		$this->visibilityService = GeneralUtility::makeInstance(VisibilityService::class);
		$this->visibilityFlagRepository = GeneralUtility::makeInstance(VisibilityFlagRepository::class);
		$this->siteFinder = GeneralUtility::makeInstance(SiteFinder::class);
		$this->connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
	}

	/**
	 * This hook starts before the datamap processing starts
	 * It is used to sort out the incoming datamap with flag == "active"
	 * so the DataHandler does not insert these records and deletes them
	 * right after.
	 *
	 * @param DataHandler $dataHandler
	 * @throws \Doctrine\DBAL\Driver\Exception
	 * @throws \Doctrine\DBAL\Exception
	 */
	public function processDatamap_beforeStart(DataHandler $dataHandler): void {
		foreach ($dataHandler->datamap as $table => $elements) {
			if ($table !== 'tx_languagevisibility_visibility_flag') {
				continue;
			}

			foreach ($elements as $id => $fieldConfiguration) {
				if ($fieldConfiguration['flag'] !== 'active') {
					continue;
				}

				unset($dataHandler->datamap[$table][$id]);
				if (MathUtility::canBeInterpretedAsInteger($id)) {
					$newDataHandler = GeneralUtility::makeInstance(DataHandler::class);
					$newDataHandler->enableLogging = $dataHandler->enableLogging;
					$newDataHandler->start(
						[],
						[
							$table => [
								$id => [
									'delete' => 1
								]
							]
						],
						$dataHandler->BE_USER
					);
					$newDataHandler->process_cmdmap();
					$this->errorLog = array_merge(
						$dataHandler->errorLog,
						$newDataHandler->errorLog
					);
				}
				if (isset($fieldConfiguration['applyRecursive']) && $fieldConfiguration['applyRecursive']) {
					$this->applyVisibilityFlagRecursive(
						$fieldConfiguration['pid'],
						$fieldConfiguration['record_language_uid'],
						'active',
						$fieldConfiguration['enableLogging'],
						$dataHandler
					);
				}
			}
		}
	}

	/**
	 * Hook method after table data are modified.
	 *
	 * @param string $command
	 * @param string $table
	 * @param int $identity
	 * @param int|array $value
	 * @param DataHandler $dataHandler
	 * @param mixed $pasteUpdate
	 * @param mixed $pasteDatamap
	 * @return void
	 * @throws \Doctrine\DBAL\Driver\Exception
	 * @throws \Doctrine\DBAL\Exception
	 */
	public function processCmdmap_postProcess(
		string $command,
		string $table,
		int $identity,
		$value,
		DataHandler $dataHandler,
		$pasteUpdate,
		$pasteDatamap
	): void {
		if ($table === 'pages') {
			if ($command === 'copy') {
				if ($identity <= 0) {
					return;
				}

				$newPageId = (int) ($dataHandler->copyMappingArray['pages'][$identity] ?? 0);
				if ($newPageId <= 0) {
					return;
				}

				$this->copyLanguageVisibilityFlagsFromParentPage($newPageId);
			} elseif ($command === 'move') {
				$this->copyLanguageVisibilityFlagsFromParentPage($identity);
			}
		} elseif ($command === 'copy' && in_array($table, $this->visibilityService->getSupportedTables(), TRUE)) {
			$oldId = $identity;
			$newId = (int) ($dataHandler->copyMappingArray[$table][$identity] ?? 0);

			// get the pid of the new record
			$newRecord = BackendUtility::getRecord($table, $newId);
			if ($newRecord !== NULL) {
				$newPid = $newRecord['pid'];
			} else {
				return;
			}

			try {
				$this->visibilityFlagRepository->flushFlagCache();
				$site = $this->siteFinder->getSiteByPageId($newPid);
			} catch (Exception $e) {
				// really a good idea?
				return;
			}

			$availableLanguages = $site->getAllLanguages();
			$visibilityFlags = [];
			foreach ($availableLanguages as $language) {
				$lid = $language->getLanguageId();
				$visibilityFlag = $this->visibilityFlagRepository->getVisibilityFlag($table, $oldId, $lid);
				if ($visibilityFlag !== FALSE && $visibilityFlag !== NULL) {
					$visibilityFlags[$lid] = $visibilityFlag['flag'];
				}
			}

			foreach ($visibilityFlags as $lid => $flag) {
				$this->visibilityFlagRepository->setVisibilityFlag($flag, $table, $newId, $lid);
			}
		}
	}

	/**
	 * Changes the pid of the visibility flags from a moved record
	 *
	 * @param string $table
	 * @param int $uid
	 * @param int $destPid
	 * @param array $propArr
	 * @param array $moveRec
	 * @param int $resolvedPid
	 * @param mixed $recordWasMoved
	 * @param DataHandler $dataHandler
	 * @throws \Doctrine\DBAL\Exception
	 */
	public function moveRecord(
		string $table,
		int $uid,
		int $destPid,
		array $propArr,
		array $moveRec,
		int $resolvedPid,
		$recordWasMoved,
		DataHandler $dataHandler
	): void {
		if ($table === 'pages' || !in_array($table, $this->visibilityService->getSupportedTables(), TRUE)) {
			return;
		}

		try {
			$pid = $resolvedPid;
			$site = $this->siteFinder->getSiteByPageId($pid);
		} catch (Exception $e) {
			return;
		}

		$availableLanguages = $site->getAllLanguages();
		foreach ($availableLanguages as $language) {
			$lid = $language->getLanguageId();

			$this->visibilityFlagRepository->flushFlagCache();
			$visibilityFlag = $this->visibilityFlagRepository->getVisibilityFlag($table, $uid, $lid);

			if ($visibilityFlag !== FALSE && $visibilityFlag !== NULL) {
				$flagUid = $visibilityFlag['uid'];
				$this->visibilityFlagRepository->moveVisibilityFlag($flagUid, $resolvedPid);
			}
		}
	}

	/**
	 * Copy languagevisibility settings from parent page.
	 *
	 * @param int $pageId
	 * @throws \Doctrine\DBAL\Exception
	 */
	protected function copyLanguageVisibilityFlagsFromParentPage(int $pageId): void {
		$page = BackendUtility::getRecord('pages', $pageId);

		if (!$page || count($page) <= 0 ||
			(isset($page[$GLOBALS['TCA']['pages']['ctrl']['languageField']]) &&
				$page[$GLOBALS['TCA']['pages']['ctrl']['languageField']] > 0)
		) {
			return;
		}

		$parentPage = BackendUtility::getRecord('pages', $page['pid']);
		if (!$parentPage || count($parentPage) <= 0) {
			return;
		}

		$parentPageUid = $parentPage['uid'];
		try {
			$rootline = BackendUtility::BEgetRootLine($pageId);
			$site = $this->siteFinder->getSiteByPageId($pageId, $rootline);
		} catch (Exception $e) {
			return;
		}

		$availableLanguages = $site->getAllLanguages();

		// clear the existing/automatically copied flags on the copied/inserted/moved page
		$this->visibilityFlagRepository->removeFlagsByTableAndPid('pages', $pageId);
		$this->visibilityFlagRepository->flushFlagCache();

		if ($availableLanguages && count($availableLanguages) > 0) {
			// collect the flags of the parent page and its overlay
			foreach ($availableLanguages as $language) {
				$lid = $language->getLanguageId();

				// get visibility from parent page
				$flagRow = $this->visibilityFlagRepository->getVisibilityFlag('pages', $parentPageUid, $lid);

				if ($flagRow !== NULL && isset($flagRow['flag'])) {
					$flag = $flagRow['flag'];
				} else {
					$flag = 'active';
				}

				// since the data mapper automatically copies the flag records,
				$queryBuilder = $this->connectionPool->getQueryBuilderForTable(
					'tx_languagevisibility_visibility_flag'
				);

				$queryBuilder
					->insert('tx_languagevisibility_visibility_flag')
					->values(
						[
							'pid' => $pageId,
							'flag' => $flag,
							'record_uid' => 'pages_' . $pageId,
							'record_language_uid' => $lid,
							'tstamp' => time(),
							'crdate' => time(),
							'record_table' => 'pages',
						]
					)
					->executeStatement();
			}
		}
	}

	/**
	 * This function is called by TYPO3 each time an element is saved in the backend
	 *
	 * @param array $incomingFieldArray
	 * @param string $table
	 * @param string $id
	 * @param DataHandler $dataHandler
	 * @throws \Doctrine\DBAL\Driver\Exception
	 * @throws \Doctrine\DBAL\Exception
	 */
	public function processDatamap_preProcessFieldArray(
		array &$incomingFieldArray,
		string $table,
		string $id,
		DataHandler $dataHandler
	): void {
		if ($table !== 'tx_languagevisibility_visibility_flag') {
			return;
		}

		$recordTable = $incomingFieldArray['record_table'];
		if (!in_array($recordTable, $this->visibilityService->getSupportedTables(), TRUE)) {
			return;
		}

		$enableLogging = FALSE;
		if (isset($incomingFieldArray['enableLogging'])) {
			$enableLogging = (bool) $incomingFieldArray['enableLogging'];
			unset($incomingFieldArray['enableLogging']);
		}

		$applyRecursive = FALSE;
		if (isset($incomingFieldArray['applyRecursive'])) {
			$applyRecursive = (bool) $incomingFieldArray['applyRecursive'];
			unset($incomingFieldArray['applyRecursive']);
		}

		$languageId = -1;
		if (isset($incomingFieldArray['record_language_uid'])) {
			$languageId = (int) $incomingFieldArray['record_language_uid'];
		}

		$pid = 0;
		if (isset($incomingFieldArray['pid'])) {
			$pid = (int) $incomingFieldArray['pid'];
		}

		$visibilityFlag = '';
		if (isset($incomingFieldArray['flag'])) {
			$visibilityFlag = (string) $incomingFieldArray['flag'];
		}

		if ($recordTable === 'pages' && $applyRecursive) {
			$this->applyVisibilityFlagRecursive(
				$pid,
				$languageId,
				$visibilityFlag,
				$enableLogging,
				$dataHandler
			);
		}
	}

	/**
	 * Applies the given language data recursive.
	 *
	 * @param int $pageId
	 * @param int $languageId
	 * @param string $visibilityFlag
	 * @param boolean $enableLogging
	 * @param DataHandler $dataHandler
	 * @return void
	 * @throws \Doctrine\DBAL\Driver\Exception
	 * @throws \Doctrine\DBAL\Exception
	 */
	protected function applyVisibilityFlagRecursive(
		int $pageId,
		int $languageId,
		string $visibilityFlag,
		bool $enableLogging,
		DataHandler $dataHandler
	): void {
		if ($pageId <= 0 || $languageId < 0 || strlen($visibilityFlag) <= 0) {
			return;
		}

		$queryBuilder = $this->connectionPool->getQueryBuilderForTable('pages');
		$queryBuilder->getRestrictions()->removeAll()->add(GeneralUtility::makeInstance(DeletedRestriction::class));

		$childPages = $queryBuilder->select('*')
			->from('pages')
			->where(
				$queryBuilder->expr()->eq('pid', $queryBuilder->createNamedParameter($pageId, \PDO::PARAM_INT))
			)
			->andWhere(
				$queryBuilder->expr()->eq(
					'sys_language_uid',
					$queryBuilder->createNamedParameter(0, \PDO::PARAM_INT)
				)
			)
			->executeQuery()->fetchAllAssociative();

		if (count($childPages) <= 0) {
			return;
		}

		foreach ($childPages as $childPage) {
			$childPageUid = (int) $childPage['uid'];

			$this->visibilityFlagRepository->flushFlagCache();
			$existingVisibilityFlag = $this->visibilityFlagRepository->getVisibilityFlag(
				'pages',
				$childPageUid,
				$languageId
			);

			if (!is_null($existingVisibilityFlag) && $existingVisibilityFlag['uid'] > 0) {
				//flag already exists and only needs to change
				$flagUid = $existingVisibilityFlag['uid'];

				if ($enableLogging) {
					// Prepare the history for logging.
					$dataHandler->compareFieldArrayWithCurrentAndUnset(
						'tx_languagevisibility_visibility_flag',
						$flagUid,
						$existingVisibilityFlag
					);
					if ($visibilityFlag === 'active') {
						$dataHandler->deleteEl(
							'tx_languagevisibility_visibility_flag',
							$flagUid
						);
					} else {
						$dataHandler->updateDB(
							'tx_languagevisibility_visibility_flag',
							$flagUid,
							['flag' => $visibilityFlag]
						);
					}
				} else {
					if ($visibilityFlag === 'active') {
						$this->visibilityFlagRepository->deleteVisibilityFlag(
							'pages',
							$childPageUid,
							$languageId
						);
					} else {
						$this->visibilityFlagRepository->setVisibilityFlag(
							$visibilityFlag,
							'pages',
							$childPageUid,
							$languageId
						);
					}
				}
			} else {
				//visibility flag record doesn't exist and needs to be created
				$newUid = StringUtility::getUniqueId('NEW');

				if ($visibilityFlag !== 'active') {
					if ($enableLogging) {
						$dataHandler->insertDB(
							'tx_languagevisibility_visibility_flag',
							$newUid,
							[
								'tstamp' => time(),
								'crdate' => time(),
								'flag' => $visibilityFlag,
								'pid' => $childPageUid,
								'record_table' => 'pages',
								'record_uid' => 'pages_' . $childPageUid,
								'record_language_uid' => $languageId
							]
						);
					} else {
						$this->visibilityFlagRepository->setVisibilityFlag(
							$visibilityFlag,
							'pages',
							$childPageUid,
							$languageId
						);
					}
				}
			}

			$this->applyVisibilityFlagRecursive(
				$childPageUid,
				$languageId,
				$visibilityFlag,
				$enableLogging,
				$dataHandler
			);
		}
	}

	/**
	 * This method is used to initialize new Elements with the default
	 *
	 * @param string $status
	 * @param string $table
	 * @param string $id
	 * @param array $fieldArray
	 * @param DataHandler $dataHandler
	 * @throws \Doctrine\DBAL\Exception
	 */
	public function processDatamap_afterDatabaseOperations(
		string $status,
		string $table,
		string $id,
		array $fieldArray,
		DataHandler $dataHandler
	): void {
		if ($table === 'pages' && $status === 'new') {
			$row = [];
			if (is_numeric($id) && $id > 0) {
				$row['uid'] = $id;
			} else {
				$row['uid'] = $dataHandler->substNEWwithIDs[$id] ?? NULL;
			}

			if ($row['uid'] !== NULL) {
				$this->copyLanguageVisibilityFlagsFromParentPage((int) $row['uid']);
			}
		}
	}

	/**
	 * Removes the default flags after all database operations
	 *
	 * @param DataHandler $dataHandler
	 * @throws \Doctrine\DBAL\Exception
	 */
	public function processCmdmap_afterFinish(DataHandler $dataHandler): void {
		$this->visibilityFlagRepository->deleteDefaultFlags();
	}
}
