<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace TYPO3\Languagevisibility\Repository;

use Doctrine\DBAL\Exception;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\DataHandling\DataHandler;
use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * This class provides the necessary database access function to fetch the visibility flags
 *
 * @author Fabio Stegmeyer <fabio.stegmeyer@sgalinski.de>
 */
class VisibilityFlagRepository implements SingletonInterface {
	/**
	 * @var array
	 */
	protected array $cachedFlags = [];

	/**
	 * @var ConnectionPool
	 */
	protected ConnectionPool $connectionPool;

	public function __construct(ConnectionPool $connectionPool) {
		$this->connectionPool = $connectionPool;
	}

	/**
	 * This option must be used e.g. when flags are saved or edited, since the first call of the getVisibility function
	 * happens before the new/edited flags are saved to the DB, therefore they are missing/wrong in the cache when the
	 * function is called later in the same request with the intention to retrieve the existing flags
	 *
	 * IMPORTANT: THIS IS EXTREMELY DANGEROUS IN TERMS OF PERFORMANCE! ONLY CALL IF THERE IS NO OTHER OPTION
	 * LIKE DIRECTLY CHANGING THE VALUE IN THE CACHE ITSELF!!!
	 *
	 * @return void
	 */
	public function flushFlagCache(): void {
		$this->cachedFlags = [];
	}

	/**
	 * Inserts or updates a flag for a given element
	 *
	 * @param string $flag
	 * @param string $table
	 * @param int $recordUid
	 * @param int $lUid
	 * @return void
	 * @throws Exception
	 */
	public function setVisibilityFlag(string $flag, string $table, int $recordUid, int $lUid): void {
		$existingVisibilityFlag = $this->getVisibilityFlag($table, $recordUid, $lUid);
		$queryBuilder = $this->connectionPool->getQueryBuilderForTable(
			'tx_languagevisibility_visibility_flag'
		);

		$pid = 0;
		if ($table === 'pages') {
			$pid = $recordUid;
		} else {
			$record = BackendUtility::getRecord($table, $recordUid);
			if ($record !== NULL) {
				$pid = $record['pid'];
			}
		}

		if ($existingVisibilityFlag !== NULL) {
			// flag already exists and only needs to be changed
			$uid = (int) $existingVisibilityFlag['uid'];

			$queryBuilder
				->update('tx_languagevisibility_visibility_flag')
				->where($queryBuilder->expr()->eq('uid', $uid))
				->set('flag', $flag)
				->executeStatement();
		} else {
			//flag doesn't exist and needs to be created
			$queryBuilder
				->insert('tx_languagevisibility_visibility_flag')
				->values(
					[
						'pid' => $pid,
						'flag' => $flag,
						'record_table' => $table,
						'record_uid' => $table . '_' . $recordUid,
						'record_language_uid' => $lUid,
						'tstamp' => time(),
						'crdate' => time()
					]
				)
				->executeQuery();
		}

		$this->flushFlagCache();
	}

	/**
	 * Moves the visibility flag to another pid
	 *
	 * @param int $uid
	 * @param int $destPid
	 * @return void
	 */
	public function moveVisibilityFlag(int $uid, int $destPid): void {
		$queryBuilder = $this->connectionPool->getQueryBuilderForTable(
			'tx_languagevisibility_visibility_flag'
		);

		$queryBuilder
			->update('tx_languagevisibility_visibility_flag')
			->where($queryBuilder->expr()->eq('uid', $uid))
			->set('pid', $destPid)
			->executeStatement();
	}

	/**
	 * Returns the requested visibility flag
	 *
	 * IMPORTANT: DON'T THINK ABOUT INTRODUCING A CACHE IGNORE RULE!!! THIS CALL IS EXTREMELY EXPENSIVE!
	 *
	 * @param string $table
	 * @param int $recordUid
	 * @param int $languageUid
	 * @return array|NULL
	 * @throws Exception
	 */
	public function getVisibilityFlag(string $table, int $recordUid, int $languageUid): ?array {
		if ($this->cachedFlags === []) {
			$queryBuilder = $this->connectionPool->getConnectionForTable(
				'tx_languagevisibility_visibility_flag'
			);
			// Fetching all the records in bulk, because of performance
			$results = $queryBuilder->prepare(
				'SELECT *, CONCAT_WS("-", record_table, record_language_uid, record_uid)
				as hash FROM tx_languagevisibility_visibility_flag'
			);

			if ($results = $results->executeQuery()) {
				$fetchedResults = $results->fetchAllAssociative();
				foreach ($fetchedResults as $entry) {
					$this->cachedFlags[$entry['hash']] = $entry;
				}

				unset($fetchedResults, $results);
			}
		}

		$flagEntry = NULL;
		// Note: record_uid = <table>_<id>
		$cacheKey = $table . '-' . $languageUid . '-' . ($table . '_' . $recordUid);
		if (isset($this->cachedFlags[$cacheKey])) {
			$flagEntry = $this->cachedFlags[$cacheKey];
		}

		return $flagEntry;
	}

	/**
	 * Delete the visibility flag for a given record and its language
	 *
	 * @param string $table The record table
	 * @param int $recordUid The record uid
	 * @param int $languageUid The language id
	 */
	public function deleteVisibilityFlag(string $table, int $recordUid, int $languageUid): void {
		$connection = $this->connectionPool->getConnectionForTable('tx_languagevisibility_visibility_flag');
		$connection->delete(
			'tx_languagevisibility_visibility_flag',
			[
				'record_uid' => $recordUid,
				'record_language_uid' => $languageUid,
				'record_table' => $table
			]
		);
	}

	/**
	 * Removes the flags for the given table and pid.
	 *
	 * @param string $table
	 * @param int $pid
	 * @return void
	 * @throws Exception
	 */
	public function removeFlagsByTableAndPid(string $table, int $pid): void {
		$queryBuilder = $this->connectionPool->getQueryBuilderForTable(
			'tx_languagevisibility_visibility_flag'
		);
		$result = $queryBuilder->select('uid')
			->from('tx_languagevisibility_visibility_flag')
			->where(
				$queryBuilder->expr()->eq(
					'pid',
					$queryBuilder->createNamedParameter($pid, \PDO::PARAM_INT)
				)
			)
			->andWhere(
				$queryBuilder->expr()->eq(
					'record_table',
					$queryBuilder->createNamedParameter($table, \PDO::PARAM_STR)
				)
			)
			->executeQuery();
		$rows = $result->fetchAllAssociative();

		$dataHandler = GeneralUtility::makeInstance(DataHandler::class);
		$dataHandler->start([], []);
		foreach ($rows as $row) {
			$dataHandler->deleteAction('tx_languagevisibility_visibility_flag', $row['uid']);
		}

		$this->flushFlagCache();
	}

	/**
	 * This function is called after TceMainHook operation to delete all default-flags
	 *
	 * @return void
	 * @throws Exception
	 */
	public function deleteDefaultFlags(): void {
		$queryBuilder = $this->connectionPool->getQueryBuilderForTable(
			'tx_languagevisibility_visibility_flag'
		);
		$result = $queryBuilder->select('uid')
			->from('tx_languagevisibility_visibility_flag')
			->where(
				$queryBuilder->expr()->eq('flag', $queryBuilder->createNamedParameter('active'))
			)
			->executeQuery();
		$rows = $result->fetchAllAssociative();
		$dataHandler = GeneralUtility::makeInstance(DataHandler::class);
		$dataHandler->start([], []);
		foreach ($rows as $row) {
			$dataHandler->deleteAction('tx_languagevisibility_visibility_flag', $row['uid']);
		}

		$this->flushFlagCache();
	}
}
