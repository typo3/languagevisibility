<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace TYPO3\Languagevisibility\Middleware;

use Exception;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Context\LanguageAspect;
use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Error\Http\PageNotFoundException;
use TYPO3\CMS\Core\Routing\PageArguments;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\Controller\ErrorController;
use TYPO3\CMS\Frontend\Page\PageAccessFailureReasons;
use TYPO3\Languagevisibility\Service\FrontendServices;

/**
 * Middleware to resolve the languagevisibility of the currently called page and if the page should not be visible
 * throw a 404 error (aka page not found action)
 *
 * @package SGalinski\LanguageVisibility\Middleware
 * @author Kevin Ditscheid <kevin.ditscheid@sgalinski.de>
 */
class LanguageVisibilityResolver implements MiddlewareInterface {
	/**
	 * @var Context
	 */
	protected Context $context;

	/**
	 * @var ConnectionPool
	 */
	protected ConnectionPool $connectionPool;

	/**
	 * @var FrontendServices
	 */
	protected FrontendServices $frontendServices;

	/**
	 * LanguageVisibilityResolver constructor.
	 */
	public function __construct(
		Context $context,
		ConnectionPool $connectionPool,
		FrontendServices $frontendServices
	) {
		$this->context = $context;
		$this->connectionPool = $connectionPool;
		$this->frontendServices = $frontendServices;
	}

	/**
	 * Process an incoming server request.
	 *
	 * Processes an incoming server request in order to produce a response.
	 * If unable to produce the response itself, it may delegate to the provided
	 * request handler to do so.
	 *
	 * @param ServerRequestInterface $request
	 * @param RequestHandlerInterface $handler
	 * @return ResponseInterface
	 * @throws PageNotFoundException
	 * @throws Exception|\Doctrine\DBAL\Driver\Exception
	 */
	public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface {
		$language = $request->getAttribute('language');
		$languageId = $language->getLanguageId();
		// should not be required and can cause several issues (e.g. with the language menu)
		// $this->context->setAspect('language', new LanguageAspect($languageId));
		$pageArguments = $request->getAttribute('routing');
		if ($pageArguments instanceof PageArguments) {
			$query = $this->connectionPool->getQueryBuilderForTable('pages');
			$query->getRestrictions()->removeAll();
			$pageId = $pageArguments->getPageId();
			$page = $query->select('*')
				->from('pages')
				->where(
					$query->expr()->eq(
						'uid',
						$query->createNamedParameter($pageId, Connection::PARAM_INT)
					)
				)->executeQuery()->fetchAssociative();

			// need to check access for current page and show error:
			if (!$page || !$this->frontendServices->checkVisiblityForElement($page, 'pages', $languageId)) {
				return GeneralUtility::makeInstance(ErrorController::class)->pageNotFoundAction(
					$request,
					'The requested page does not exist',
					['code' => PageAccessFailureReasons::PAGE_NOT_FOUND]
				);
			}
		}

		return $handler->handle($request);
	}
}
