<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace TYPO3\Languagevisibility\Service;

use Exception;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Site\SiteFinder;
use TYPO3\Languagevisibility\Element\Element;
use TYPO3\Languagevisibility\Element\ElementFactory;
use TYPO3\Languagevisibility\Exception\TableNotSupportedException;
use TYPO3\Languagevisibility\Repository\VisibilityFlagRepository;

/**
 * Service class for languagevisibility frontend logics
 */
class FrontendServices extends AbstractServices {
	/**
	 * @var VisibilityService
	 */
	protected VisibilityService $visibilityService;
	/**
	 * @var VisibilityFlagRepository $visibilityFlagRepository
	 */
	protected VisibilityFlagRepository $visibilityFlagRepository;

	/**
	 * @var ElementFactory
	 */
	protected ElementFactory $elementFactory;

	/**
	 * @var SiteFinder
	 */
	protected SiteFinder $siteFinder;

	public function __construct(
		VisibilityService $visibilityService,
		VisibilityFlagRepository $visibilityFlagRepository,
		ElementFactory $elementFactory,
		SiteFinder $siteFinder
	) {
		$this->visibilityService = $visibilityService;
		$this->visibilityFlagRepository = $visibilityFlagRepository;
		$this->elementFactory = $elementFactory;
		$this->siteFinder = $siteFinder;
	}

	/**
	 * Returns true if the element should be visible with the given language visibility settings
	 *
	 * @param array $row
	 * @param string $table
	 * @param int $languageId
	 * @return bool
	 * @throws Exception|\Doctrine\DBAL\Driver\Exception
	 */
	public function checkVisiblityForElement(array $row, string $table, int $languageId): bool {
		$visibilityFlag = $this->visibilityFlagRepository->getVisibilityFlag($table, (int) $row['uid'], $languageId);
		if ($visibilityFlag) {
			$visibilityFlag = $visibilityFlag['flag'];
		}

		$l10nParent = $row['l10n_parent'] ?? 0;
		if (!$visibilityFlag && $l10nParent > 0) {
			// This is needed for translations, if the row isn't the default language.
			$row['uid'] = $row['l10n_parent'];
			$row['l10n_parent'] = 0;
			return $this->checkVisiblityForElement($row, $table, $languageId);
		}

		if (!$visibilityFlag || $visibilityFlag === 'active') {
			return TRUE;
		}

		if ($visibilityFlag === 'inactive') {
			return FALSE;
		}

		// CASE: active, if translated...
		// if the visibility flag isn't straight forward, we use the element-object to resolve the visibility
		$element = $this->elementFactory->getElementForTable($table, $row);

		if ($table === 'pages') {
			$pid = $row['uid'];
		} else {
			$pid = $row['pid'];
		}

		try {
			$rootLine = BackendUtility::BEgetRootLine($pid);
			$site = $this->siteFinder->getSiteByPageId($pid, $rootLine);
			$language = $site->getLanguageById($languageId);
		} catch (Exception $e) {
			return FALSE;
		}

		return $this->visibilityService->resolveVisibility($visibilityFlag, $language, $element);
	}

	/**
	 * Returns the Element object based on the given data
	 *
	 * @param array $row
	 * @param string $table
	 * @return Element
	 * @throws TableNotSupportedException
	 * @throws \Doctrine\DBAL\Exception
	 */
	public function getElement(array $row, string $table): Element {
		return $this->elementFactory->getElementForTable($table, $row);
	}

	/**
	 * Get the language id of the language overlay for the given element
	 *
	 * @param Element $element
	 * @param int $languageId
	 * @return bool|int|NULL
	 */
	public function getOverlayLanguageIdForElement(Element $element, int $languageId): bool|int|NULL {
		if ($element->getTable() === 'pages') {
			$pid = $element->getUid();
		} else {
			$pid = $element->getPid();
		}

		try {
			$site = $this->siteFinder->getSiteByPageId($pid);
			$language = $site->getLanguageById($languageId);
		} catch (Exception $exception) {
			return FALSE;
		}

		return $this->visibilityService->getOverlayLanguageIdForLanguageAndElement($language, $element);
	}
}
