<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace TYPO3\Languagevisibility\Service;

use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Site\Entity\SiteLanguage;
use TYPO3\Languagevisibility\Element\Element;
use TYPO3\Languagevisibility\Element\Visibility;

/**
 * Service class that handles visibility functionality
 */
class VisibilityService implements SingletonInterface {
	/**
	 * @var array $supportedTables
	 */
	private array $supportedTables = [];

	/**
	 * Returns relevant languageId for overlay record or FALSE if element is not visible for given language
	 *
	 * @param SiteLanguage $language
	 * @param Element $element
	 * @return int|NULL
	 */
	public function getOverlayLanguageIdForLanguageAndElement(SiteLanguage $language, Element $element): ?int {
		if ($this->isVisible($language, $element)) {
			return $language->getLanguageId();
		}

		return NULL;
	}

	/**
	 * Gets the tables configured with language visibility support.
	 *
	 * @return array with all supported tables
	 */
	public function getSupportedTables(): array {
		if ($this->supportedTables === []) {
			$this->supportedTables = ['pages', 'tt_content'];

			if (isset($GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['languagevisibility']['getElementForTable'])
				&& is_array($GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['languagevisibility']['getElementForTable'])
			) {
				$this->supportedTables = array_merge(
					$this->supportedTables,
					array_keys($GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['languagevisibility']['getElementForTable'])
				);
			}

			if (isset($GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['languagevisibility']['recordElementSupportedTables'])
				&& is_array(
					$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['languagevisibility']['recordElementSupportedTables']
				)
			) {
				$this->supportedTables = array_merge(
					$this->supportedTables,
					array_keys(
						$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['languagevisibility']['recordElementSupportedTables']
					)
				);
			}
		}

		return $this->supportedTables;
	}

	/**
	 * Method to check if records of a given table support the language-visibility feature.
	 *
	 * @param string $table
	 * @return bool
	 */
	public function isSupportedTable(string $table): bool {
		return in_array($table, $this->getSupportedTables(), TRUE);
	}

	/**
	 * Returns true if the element is visible
	 *
	 * @param SiteLanguage $language
	 * @param Element $element
	 * @return bool
	 */
	public function isVisible(SiteLanguage $language, Element $element): bool {
		$visibility = $this->getVisibilitySetting($language, $element);
		return $this->resolveVisibility($visibility, $language, $element);
	}

	/**
	 * Resolves the visibility and returns TRUE or FALSE
	 *
	 * @param string $visibility
	 * @param SiteLanguage $language
	 * @param Element $element
	 * @return bool
	 */
	public function resolveVisibility(string $visibility, SiteLanguage $language, Element $element): bool {
		if ($visibility === '' || $visibility === 'active') {
			return TRUE;
		}

		if ($visibility === 'inactive') {
			return FALSE;
		}

		if ($visibility === 'translated') {
			return $element->hasTranslation($language->getLanguageId());
		}

		return TRUE;
	}

	/**
	 * Return the accumulated visibility setting: reads default for language then reads local for element and merges them.
	 * if local is default, then the global is used or it is forced to be "yes" if the language was set to all.
	 * if the element itself is a translated original record the element is only visible in the specific language
	 * If nothing is set the hardcoded default "t" (translated) is returned
	 *
	 * @param SiteLanguage $language
	 * @param Element $element
	 * @return string
	 */
	public function getVisibilitySetting(SiteLanguage $language, Element $element): string {
		return $this->getVisibility($language, $element)->getVisibilityString();
	}

	/**
	 * This method can be used to retrieve an informal description for the visibility of an element
	 *
	 * @param SiteLanguage $language
	 * @param Element $element
	 * @return string
	 */
	public function getVisibilityDescription(SiteLanguage $language, Element $element): string {
		return $this->getVisibility($language, $element)->getVisibilityDescription();
	}

	/**
	 * Create a Visibility object for an element for a given language.
	 *
	 * @param SiteLanguage $language
	 * @param Element $element
	 * @return Visibility
	 */
	protected function getVisibility(SiteLanguage $language, Element $element): Visibility {
		$visibility = new Visibility();
		$local = $element->getVisibilitySettingForLanguage($language->getLanguageId());
		if ($local !== '' && $local !== 'active') {
			$visibility->setVisibilityString($local)->setVisibilityDescription('local setting ' . $local);
		} elseif ($element->isLanguageSetToAll()) {
			$visibility->setVisibilityString('active')->setVisibilityDescription('language configured to all');
		} elseif ($element->isMonolithicTranslated()) {
			if ($element->languageEquals($language)) {
				$visibility->setVisibilityString('active')->setVisibilityDescription('');
			} else {
				$visibility->setVisibilityString('inactive')->setVisibilityDescription('');
			}
		} else {
			$visibility->setVisibilityString('active')->setVisibilityDescription('default');
		}

		return $visibility;
	}
}
