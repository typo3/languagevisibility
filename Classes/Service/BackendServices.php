<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace TYPO3\Languagevisibility\Service;

use TYPO3\CMS\Core\Authentication\BackendUserAuthentication;
use TYPO3\CMS\Core\Site\Entity\SiteLanguage;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

/**
 * Class BackendServices
 */
class BackendServices extends AbstractServices {
	/**
	 * @var VisibilityService
	 */
	protected VisibilityService $visibilityService;
	/**
	 * @var BackendUserAuthentication
	 */
	protected BackendUserAuthentication $backendUserAuthentication;

	public function __construct(
		VisibilityService $visibilityService,
		BackendUserAuthentication $backendUserAuthentication
	) {
		$this->visibilityService = $visibilityService;
		$this->backendUserAuthentication = $backendUserAuthentication;
	}

	/**
	 * Helper function to check if a record from a given table in an overlay record
	 *
	 * @param array $row
	 * @param string $table
	 * @return bool
	 */
	public function isOverlayRecord(array $row, string $table): bool {
		$result = FALSE;

		if (in_array($table, $this->visibilityService->getSupportedTables(), TRUE)) {
			$translationIdField = $GLOBALS['TCA'][$table]['ctrl']['transOrigPointerField'];
			if ($translationIdField !== '') {
				// if the field which points to the original of the translation is
				// not 0 a translation exists, and we have an overlay record
				if (is_array($row[$translationIdField])) {
					// somehow the sys_language_uid field could contain 0 => 0
					$result = (int) $row[$translationIdField][0] !== 0;
				} else {
					$result = (int) $row[$translationIdField] !== 0;
				}
			}
		}

		return $result;
	}

	/**
	 * Returns array with the visibility options that are allowed for the current user.
	 *
	 * @param SiteLanguage $language
	 * @param bool $isOverlay
	 * @return array
	 */
	public function getAvailableOptionsForLanguage(SiteLanguage $language, bool $isOverlay = FALSE): array {
		$languageId = $language->getLanguageId();
		$select = [];

		$select['active'] = 'active';
		if (!$isOverlay) {
			if ($languageId !== 0) {
				$select['translated'] = 'translated';
			}
			$select['inactive'] = 'inactive';

			// check permissions, if user has no permission only no for the language is allowed
			// if the user has permissions for languages that act as fallback language: then the languages that falls back can have "-" in the options!
			if (!$this->backendUserAuthentication->checkLanguageAccess($languageId)) {
				//check if the language falls back to one of the languages the user has permissions:
				$isInFallback = FALSE;
				$fallbacks = $language->getFallbackLanguageIds();

				foreach ($fallbacks as $fallbackLanguageId) {
					if ($this->backendUserAuthentication->checkLanguageAccess($fallbackLanguageId)) {
						$isInFallback = TRUE;
					}
				}
				$select = [];
				if ($isInFallback) {
					$select['active'] = 'active';
				}

				$select['inactive'] = 'inactive';
			}
		} else {
			// overlays elements can only have "force to no"
			$select['inactive'] = 'inactive';
		}

		// Get translations of labels from the locallang file
		foreach ($select as $k => $v) {
			$select[$k] = LocalizationUtility::translate(
				'LLL:EXT:languagevisibility/Resources/Private/Language/locallang_db.xlf:tx_languagevisibility_visibility.I.' . $v,
				'languagevisibility'
			);
		}

		return $select;
	}
}
