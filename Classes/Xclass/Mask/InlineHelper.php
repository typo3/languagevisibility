<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace TYPO3\Languagevisibility\Xclass\Mask;

use Doctrine\DBAL\Driver\Exception;
use MASK\Mask\Definition\TableDefinitionCollection;
use MASK\Mask\Domain\Repository\BackendLayoutRepository;
use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Context\Exception\AspectNotFoundException;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\Languagevisibility\Service\FrontendServices;
use TYPO3\Languagevisibility\Service\VisibilityService;

/**
 * Implements the languagevisibility check for Mask inline elements. This XClass should be saved, because it won't drop
 * the origin implementation of functions. Will just apply, if the Frontend is loaded.
 */
class InlineHelper extends \MASK\Mask\Helper\InlineHelper {
	/**
	 * @var VisibilityService
	 */
	protected VisibilityService $visibilityService;

	/**
	 * @var FrontendServices
	 */
	protected FrontendServices $frontendServices;

	/**
	 * @var Context
	 */
	protected Context $context;

	public function __construct(
		TableDefinitionCollection $tableDefinitionCollection,
		BackendLayoutRepository $backendLayoutRepository
	) {
		parent::__construct($tableDefinitionCollection, $backendLayoutRepository);
		$this->visibilityService = GeneralUtility::makeInstance(VisibilityService::class);
		$this->frontendServices = GeneralUtility::makeInstance(FrontendServices::class);
		$this->context = GeneralUtility::makeInstance(Context::class);
	}

	/**
	 * Applies the languagevisibility check for the inline elements, which were returned from the origin function.
	 *
	 * @param array $data
	 * @param string $name
	 * @param string $cType
	 * @param string $parentFieldName
	 * @param string $parenttable
	 * @param string|null $childTable
	 * @param string $originalTable
	 * @return array all irre elements of this attribut
	 * @throws AspectNotFoundException
	 * @throws Exception
	 */
	public function getInlineElements(
		array $data,
		string $name,
		string $cType,
		string $parentFieldName = 'parentid',
		string $parenttable = 'tt_content',
		?string $childTable = NULL,
		string $originalTable = 'tt_content'
	): array {
		$elements = parent::getInlineElements($data, $name, $cType, $parentFieldName, $parenttable, $childTable);
		if (count($elements) <= 0) {
			return [];
		}

		if (!isset($GLOBALS['TSFE']) || !is_object($GLOBALS['TSFE'])) {
			return $elements;
		}

		// If the child table is not set, take the name. Same logic like in the origin function.
		if (!$childTable) {
			$childTable = $name;
		}

		if (!$this->visibilityService->isSupportedTable($childTable)) {
			return $elements;
		}

		$sysLanguageUid = $this->context->getPropertyFromAspect('language', 'id');
		$filteredElements = [];
		foreach ($elements as $element) {
			try {
				$isVisible = $this->frontendServices->checkVisiblityForElement($element, $childTable, $sysLanguageUid);
			} catch (\Exception $exception) {
				$isVisible = FALSE;
			}

			if (!$isVisible) {
				// Element not visible
				continue;
			}

			$filteredElements[] = $element;
		}

		return $filteredElements;
	}
}
