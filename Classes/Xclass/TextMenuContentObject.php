<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace TYPO3\Languagevisibility\Xclass;

use Exception;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\ContentObject\Menu\TextMenuContentObject as FrontendTextMenuContentObject;
use TYPO3\Languagevisibility\Service\FrontendServices;

/**
 * Xclass for the menu renderer. The USERDEF state needs to be checked in relation to languagevisibility.
 */
class TextMenuContentObject extends FrontendTextMenuContentObject {
	public const ITEM_STATE_VISIBLE = 'NO';
	public const ITEM_STATE_INVISIBLE = 'USERDEF1';
	public const ITEM_STATE_SELECTED = 'USERDEF2';

	/**
	 * @var FrontendServices
	 */
	protected FrontendServices $frontendServices;

	public function __construct() {
		$this->frontendServices = GeneralUtility::makeInstance(FrontendServices::class);
	}

	/**
	 * Fetches all menuitems if special = language is set
	 *
	 * @param string $specialValue The value from special.value
	 * @return array
	 * @throws Exception|\Doctrine\DBAL\Driver\Exception
	 */
	protected function prepareMenuItemsForLanguageMenu($specialValue): array {
		$menuItems = parent::prepareMenuItemsForLanguageMenu($specialValue);

		$languageItems = GeneralUtility::intExplode(',', $specialValue);
		foreach ($menuItems as $index => &$menuItem) {
			$currentItemState = $menuItem['ITEM_STATE'];
			if ($currentItemState === self::ITEM_STATE_SELECTED) {
				continue;
			}

			$uid = (int) $menuItem['uid'];
			$languageUid = $languageItems[$index] ?? NULL;
			if ($uid <= 0 || $languageUid < 0 || empty($currentItemState) || $languageUid === NULL) {
				continue;
			}

			$isVisible = $this->frontendServices->checkVisiblityForElement($menuItem, 'pages', $languageUid);
			if ($isVisible && $currentItemState === self::ITEM_STATE_INVISIBLE) {
				$menuItem['ITEM_STATE'] = self::ITEM_STATE_VISIBLE;
			} elseif (!$isVisible && $currentItemState === self::ITEM_STATE_VISIBLE) {
				$menuItem['ITEM_STATE'] = self::ITEM_STATE_INVISIBLE;
			}
		}

		return $menuItems;
	}
}
