<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace TYPO3\Languagevisibility\Xclass;

use Doctrine\DBAL\Driver\Exception;
use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Context\Exception\AspectNotFoundException;
use TYPO3\CMS\Core\Domain\Repository\PageRepository as CorePageRepository;
use TYPO3\CMS\Core\Http\ApplicationType;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\Languagevisibility\Service\FrontendServices;

/**
 * Class PageRepository
 *
 * The LanguageVisibilityPageRepository is a class_alias from the ext_localconf.php.
 *
 * @package TYPO3\Languagevisibility\Xclass
 */
class PageRepository extends CorePageRepository {
	/**
	 * @var FrontendServices
	 */
	protected FrontendServices $frontendServices;

	public function __construct(Context $context = NULL) {
		$this->frontendServices = GeneralUtility::makeInstance(FrontendServices::class);
		parent::__construct($context);
	}

	/**
	 * This just checks, if the wanted page is available for the current language with the languagevisibility filters
	 * from the backend. (The Table)
	 *
	 * @param int $uid The page id to look up
	 * @param bool $disableGroupAccessCheck set to true to disable group access check
	 * @return array The resulting page record with overlays or empty array
	 * @throws \UnexpectedValueException
	 * @throws AspectNotFoundException
	 * @throws \Exception|Exception
	 */
	public function getPage($uid, $disableGroupAccessCheck = FALSE): array {
		$page = parent::getPage($uid, $disableGroupAccessCheck);
		if (count($page) <= 0) {
			return $page;
		}

		if (isset($GLOBALS['TYPO3_REQUEST']) && ApplicationType::fromRequest($GLOBALS['TYPO3_REQUEST'])->isBackend()) {
			// we are at backend context and do not want to follow up on the languagevisibility check
			return $page;
		}

		// sometimes the language can be different than this->context (technically impossible, but happens)
		//		$context = GeneralUtility::makeInstance(Context::class);
		//		$languageUid = $context->getPropertyFromAspect('language', 'id', 0);

		// Always override requests to the current called page language if you request pages for the default language
		// Fixes issues with pages that are not visible in the default language, but for the current called language
		// Issues:
		// - Content Fallbacks to the default language don't lead to a 404 if the default language is disabled in the languagevisibility flags (minor problem, because you can disable the page itself via the flags)
		$languageUid = $this->context->getPropertyFromAspect('language', 'id', 0);
		if (
			isset($GLOBALS['TYPO3_REQUEST']) && !$languageUid &&
			ApplicationType::fromRequest($GLOBALS['TYPO3_REQUEST'])->isFrontend()
		) {
			$language = $GLOBALS['TYPO3_REQUEST']->getAttribute('language');
			if ($language) {
				$languageUid = $language->getLanguageId();
			}
		}

		if ($this->frontendServices->checkVisiblityForElement($page, 'pages', $languageUid)) {
			return $page;
		}

		return [];
	}
}
