<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace TYPO3\Languagevisibility\Xclass;

use Doctrine\DBAL\Driver\Exception;
use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Context\Exception\AspectNotFoundException;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;
use TYPO3\CMS\Core\Database\Query\Restriction\WorkspaceRestriction;
use TYPO3\CMS\Core\Domain\Repository\PageRepository;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\Generic\Qom\JoinInterface;
use TYPO3\CMS\Extbase\Persistence\Generic\Qom\SelectorInterface;
use TYPO3\CMS\Extbase\Persistence\Generic\Qom\Statement;
use TYPO3\CMS\Extbase\Persistence\Generic\Storage\Exception\BadConstraintException;
use TYPO3\CMS\Extbase\Persistence\Generic\Storage\Exception\SqlErrorException;
use TYPO3\CMS\Extbase\Persistence\Generic\Storage\Typo3DbBackend as ExtbaseTypo3DbBackend;
use TYPO3\CMS\Extbase\Persistence\Generic\Storage\Typo3DbQueryParser;
use TYPO3\CMS\Extbase\Persistence\QueryInterface;
use TYPO3\CMS\Extbase\Reflection\ReflectionService;
use TYPO3\CMS\Extbase\Service\CacheService;
use TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController;
use TYPO3\Languagevisibility\Service\FrontendServices;
use TYPO3\Languagevisibility\Service\VisibilityService;

/**
 * Xclass for the TYPO3 db backend, which handles, that the l18n_cfg parameters are used for pages in Extbase.
 */
class Typo3DbBackend extends ExtbaseTypo3DbBackend {
	/**
	 * @var VisibilityService
	 */
	protected VisibilityService $visibilityService;

	/**
	 * @var FrontendServices
	 */
	protected FrontendServices $frontendServices;

	public function __construct(CacheService $cacheService, ReflectionService $reflectionService) {
		parent::__construct($cacheService, $reflectionService);
		$this->frontendServices = GeneralUtility::makeInstance(FrontendServices::class);
		$this->visibilityService = GeneralUtility::makeInstance(VisibilityService::class);
	}

	/**
	 * Returns the object data matching the $query.
	 *
	 * HINT: ONLY THE FOLLOWING LINE IS ADDED! Everything else must be synchronized in future releases.
	 * LAST SYNC: TYPO3 11.5.5, 10.4 as fallback in front
	 * ADDED LINE: $this->addLanguageVisibilityConstraint($queryBuilder, $query);
	 *
	 * @param QueryInterface $query
	 * @return array
	 * @throws AspectNotFoundException
	 * @throws SqlErrorException
	 */
	public function getObjectDataByQuery(QueryInterface $query): array {
		$statement = $query->getStatement();
		// todo: remove instanceof checks as soon as getStatement() strictly returns Qom\Statement only
		if ($statement instanceof Statement
			&& !$statement->getStatement() instanceof QueryBuilder
		) {
			$rows = $this->getObjectDataByRawQuery($statement);
		} else {
			$queryParser = GeneralUtility::makeInstance(Typo3DbQueryParser::class);
			if ($statement instanceof Statement
				&& $statement->getStatement() instanceof QueryBuilder
			) {
				$queryBuilder = $statement->getStatement();
			} else {
				$queryBuilder = $queryParser->convertQueryToDoctrineQueryBuilder($query);
			}
			$selectParts = $queryBuilder->getQueryPart('select');
			if ($queryParser->isDistinctQuerySuggested() && !empty($selectParts)) {
				$selectParts[0] = 'DISTINCT ' . $selectParts[0];
				$queryBuilder->selectLiteral(...$selectParts);
			}
			if ($query->getOffset()) {
				$queryBuilder->setFirstResult($query->getOffset());
			}
			if ($query->getLimit()) {
				$queryBuilder->setMaxResults($query->getLimit());
			}

			// Hack the language visibility constraint into the queryBuilder
			$this->addLanguageVisibilityConstraint($queryBuilder, $query);
			try {
				$rows = $queryBuilder->executeQuery()->fetchAllAssociative();
			} catch (\Doctrine\DBAL\Exception $e) {
				throw new SqlErrorException($e->getPrevious()->getMessage(), 1472074485, $e);
			}
		}

		if (!empty($rows)) {
			$rows = $this->overlayLanguageAndWorkspace($query->getSource(), $rows, $query);
		}

		return $rows;
	}

	/**
	 * Returns the number of tuples matching the query.
	 *
	 * HINT: ONLY THE FOLLOWING LINE IS ADDED! Everything else must be synchronized in future releases.
	 * LAST SYNC: TYPO3 11.5.5, 10.4 is above as fallback
	 * ADDED LINE: $this->addLanguageVisibilityConstraint($queryBuilder, $query);
	 *
	 * @param QueryInterface $query
	 * @return int The number of matching tuples
	 * @throws AspectNotFoundException
	 * @throws BadConstraintException
	 * @throws SqlErrorException
	 */
	public function getObjectCountByQuery(QueryInterface $query): int {
		if ($query->getConstraint() instanceof Statement) {
			throw new BadConstraintException('Could not execute count on queries with a constraint of type TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Qom\\Statement', 1256661045);
		}

		$statement = $query->getStatement();
		if ($statement instanceof Statement
			&& !$statement->getStatement() instanceof QueryBuilder
		) {
			$rows = $this->getObjectDataByQuery($query);
			$count = count($rows);
		} else {
			$queryParser = GeneralUtility::makeInstance(Typo3DbQueryParser::class);
			$queryBuilder = $queryParser
				->convertQueryToDoctrineQueryBuilder($query)
				->resetQueryPart('orderBy');

			if ($queryParser->isDistinctQuerySuggested()) {
				$source = $queryBuilder->getQueryPart('from')[0];
				// Tablename is already quoted for the DBMS, we need to treat table and field names separately
				$tableName = $source['alias'] ?: $source['table'];
				$fieldName = $queryBuilder->quoteIdentifier('uid');
				$queryBuilder->resetQueryPart('groupBy')
					->selectLiteral(sprintf('COUNT(DISTINCT %s.%s)', $tableName, $fieldName));
			} else {
				$queryBuilder->count('*');
			}

			// Ensure to count only records in the current workspace
			$context = GeneralUtility::makeInstance(Context::class);
			$workspaceUid = (int)$context->getPropertyFromAspect('workspace', 'id');
			$queryBuilder->getRestrictions()->add(GeneralUtility::makeInstance(WorkspaceRestriction::class, $workspaceUid));
			// Hack the language visibility constraint into the queryBuilder
			$this->addLanguageVisibilityConstraint($queryBuilder, $query);
			try {
				$count = $queryBuilder->executeQuery()->fetchOne();
			} catch (\Doctrine\DBAL\Exception $e) {
				throw new SqlErrorException($e->getPrevious()->getMessage(), 1472074379, $e);
			}

			if ($query->getOffset()) {
				$count -= $query->getOffset();
			}

			if ($query->getLimit()) {
				$count = min($count, $query->getLimit());
			}
		}

		return (int)max(0, $count);
	}

	/**
	 * This method adds the language visibility constraint to the given queryBuilder
	 * This optimally costs less time, since it uses a JOIN constraint for checking the visibility settings, which is
	 * way faster than checking every row on its own
	 *
	 * @param QueryBuilder $queryBuilder
	 * @param QueryInterface $query
	 */
	protected function addLanguageVisibilityConstraint(QueryBuilder $queryBuilder, QueryInterface $query): void {
		$tablename = '';
		$querySource = $query->getSource();
		if ($querySource instanceof SelectorInterface) {
			$tablename = $querySource->getSelectorName();
		} elseif ($querySource instanceof JoinInterface) {
			$tablename = $querySource->getRight()->getSelectorName();
		}

		if (!$this->visibilityService->isSupportedTable($tablename)) {
			return;
		}

		$languageParentField = $GLOBALS['TCA'][$tablename]['ctrl']['transOrigPointerField'];
		$languageField = $GLOBALS['TCA'][$tablename]['ctrl']['languageField'];
		if ($languageParentField === '' || $languageField === '') {
			return;
		}

		$queryBuilder->leftJoin(
			$tablename,
			'tx_languagevisibility_visibility_flag',
			'vf',
			$queryBuilder->expr()->and(
				$queryBuilder->expr()->eq('vf.record_table', $queryBuilder->createNamedParameter($tablename)),
				$queryBuilder->expr()->eq('vf.record_language_uid', $query->getQuerySettings()->getLanguageAspect()->getId()),
				$queryBuilder->expr()->eq(
					'vf.record_uid',
					'IF(' . $tablename . '.' . $languageField . ' > 0, ' .
					'CONCAT(\'' . $tablename . '_\', ' . $tablename . '.' . $languageParentField . '), ' .
					'CONCAT(\'' . $tablename . '_\', ' . $tablename . '.uid))'
				)
			)
		);

		$queryBuilder->andWhere(
			$queryBuilder->expr()->or(
				$queryBuilder->expr()->isNull('vf.flag'),
				$queryBuilder->expr()->eq('vf.flag', $queryBuilder->createNamedParameter('active')),
				$queryBuilder->expr()->eq('vf.flag', $queryBuilder->createNamedParameter('enforce')),
				$queryBuilder->expr()->and(
					$queryBuilder->expr()->gt($tablename . '.' . $languageField, 0),
					$queryBuilder->expr()->eq('vf.flag', $queryBuilder->createNamedParameter('translated'))
				)
			)
		);
	}

	/**
	 * NOTE: This function is needed, because we cannot unset the data in the getPageOverlay_preProcess
	 * Hook. One case would be the menu generation, where we need the data.
	 *
	 * @param string $tableName
	 * @param array $row
	 * @param PageRepository $pageRepository
	 * @param QueryInterface $query
	 * @return array|int|mixed|null the overlaid row or false or null if overlay failed.
	 * @throws Exception
	 */
	protected function overlayLanguageAndWorkspaceForSingleRecord(
		string $tableName,
		array $row,
		PageRepository $pageRepository,
		QueryInterface $query
	) {
		/** @noinspection CallableParameterUseCaseInTypeContextInspection */
		$row = parent::overlayLanguageAndWorkspaceForSingleRecord($tableName, $row, $pageRepository, $query);
		if (
			isset($GLOBALS['TSFE']) &&
			$row && $GLOBALS['TSFE'] instanceof TypoScriptFrontendController &&
				$this->visibilityService->isSupportedTable($tableName)
		) {
			$querySettings = $query->getQuerySettings();
			if (!$this->frontendServices->checkVisiblityForElement(
				$row,
				$tableName,
				$querySettings->getLanguageAspect()->getId()
			)) {
				// Page not visible
				/** @noinspection CallableParameterUseCaseInTypeContextInspection */
				$row = NULL;
			}
		}

		return $row;
	}
}
