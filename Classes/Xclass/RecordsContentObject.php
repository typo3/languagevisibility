<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace TYPO3\Languagevisibility\Xclass;

use Doctrine\DBAL\Driver\Exception;
use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Context\Exception\AspectNotFoundException;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\ContentObject\RecordsContentObject as FrontendRecordsContentObject;
use TYPO3\Languagevisibility\Service\FrontendServices;
use TYPO3\Languagevisibility\Service\VisibilityService;

/**
 * Will display Items based on LanguageVisibility.
 */
class RecordsContentObject extends FrontendRecordsContentObject {
	/**
	 * @var Context
	 */
	protected Context $context;

	/**
	 * @var FrontendServices
	 */
	protected FrontendServices $frontendServices;

	/**
	 * @var VisibilityService
	 */
	protected VisibilityService $visibilityService;

	public function __construct() {
		$this->context = GeneralUtility::makeInstance(Context::class);
		$this->frontendServices = GeneralUtility::makeInstance(FrontendServices::class);
		$this->visibilityService = GeneralUtility::makeInstance(VisibilityService::class);
	}

	/**
	 * Collects records according to the configured source
	 *
	 * @param string $source Source of records
	 * @param array $tables List of tables
	 * @throws Exception
	 * @throws AspectNotFoundException
	 */
	protected function collectRecordsFromSource($source, array $tables): void {
		parent::collectRecordsFromSource($source, $tables);
		$context = $this->context;
		$frontendServices = $this->frontendServices;
		$filteredData = [];
		foreach ($this->data as $table => $rows) {
			if (!$this->visibilityService->isSupportedTable($table)) {
				$filteredData[$table] = $rows;
				continue;
			}

			$filteredRows = array_filter($rows, static function ($row) use ($table, $context, $frontendServices) {
				return $frontendServices->checkVisiblityForElement(
					$row,
					$table,
					$context->getPropertyFromAspect('language', 'id')
				);
			});
			$filteredData[$table] = $filteredRows;
		}
		$this->data = $filteredData;
	}
}
