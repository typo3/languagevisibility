<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace TYPO3\Languagevisibility\Xclass;

use Exception;
use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Context\Exception\AspectNotFoundException;
use TYPO3\CMS\Core\Context\LanguageAspect;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer as FrontendContentObjectRenderer;
use TYPO3\Languagevisibility\Service\FrontendServices;
use TYPO3\Languagevisibility\Service\VisibilityService;

/**
 * Class ContentObjectRenderer
 * Note: Do not inject complicated services here, because this class is going to be serialized by some extbase logic
 *
 * @package TYPO3\Languagevisibility\Xclass
 * @author Kevin Ditscheid <kevin.ditscheid@sgalinski.de>
 */
class ContentObjectRenderer extends FrontendContentObjectRenderer {
	/**
	 * Override the getRecords method to check the language visibility
	 *
	 * @param string $tableName
	 * @param array $queryConfiguration
	 * @return array
	 * @throws AspectNotFoundException
	 * @throws Exception|\Doctrine\DBAL\Driver\Exception
	 */
	public function getRecords($tableName, array $queryConfiguration): array {
		$records = parent::getRecords($tableName, $queryConfiguration);
		/** @var VisibilityService $visibilityService */
		$visibilityService = GeneralUtility::makeInstance(VisibilityService::class);
		if (!$visibilityService->isSupportedTable($tableName)) {
			return $records;
		}

		/** @var Context $context */
		$context = GeneralUtility::makeInstance(Context::class);
		/** @var LanguageAspect $languageAspect */
		$languageAspect = $context->getAspect('language');
		$languageId = $languageAspect->getId();

		/** @var FrontendServices $frontendServices */
		$frontendServices = GeneralUtility::makeInstance(FrontendServices::class);
		$visibleRecords = [];
		foreach ($records as $record) {
			if ($frontendServices->checkVisiblityForElement($record, $tableName, $languageId)) {
				$visibleRecords[] = $record;
			}
		}

		return $visibleRecords;
	}
}
