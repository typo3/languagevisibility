<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace TYPO3\Languagevisibility\Element;

use Doctrine\DBAL\Exception;
use TYPO3\CMS\Core\Context\Exception\AspectNotFoundException;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\Restriction\DeletedRestriction;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class RecordElement
 */
class RecordElement extends Element {
	/**
	 * @return string
	 */
	public function getElementDescription(): string {
		return 'TYPO3-Record';
	}

	/**
	 * This method is the implementation of an abstract parent method.
	 * The method should return the overlay record for a certain language.
	 *
	 * (non-PHPdoc)
	 *
	 * @see classes/tx_languagevisibility_element#getOverLayRecordForCertainLanguageImplementation($languageId)
	 * @param int $languageId
	 * @return array
	 * @throws AspectNotFoundException
	 * @throws Exception
	 */
	protected function getOverLayRecordForCertainLanguageImplementation(int $languageId): array {
		if (empty($this->table)) {
			return [];
		}

		$ctrl = $GLOBALS['TCA'][$this->table]['ctrl'];
		$connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
		// Select overlay record (Live workspace, initial placeholders included):
		$queryBuilder = $connectionPool->getQueryBuilderForTable($this->table);
		$queryBuilder->getRestrictions()->removeAll()->add(GeneralUtility::makeInstance(DeletedRestriction::class));
		$queryBuilder->select('*')
			->from($this->table)
			->where(
				$queryBuilder->expr()->and(
					$queryBuilder->expr()->eq(
						'pid',
						$queryBuilder->createNamedParameter($this->getPid(), \PDO::PARAM_INT)
					),
					$queryBuilder->expr()->eq(
						$ctrl['languageField'],
						$queryBuilder->createNamedParameter($languageId, \PDO::PARAM_INT)
					)
				)
			);
		if ($languageId > 0) {
			$queryBuilder->andWhere(
				$queryBuilder->expr()->eq(
					$ctrl['transOrigPointerField'],
					$queryBuilder->createNamedParameter($this->getUid(), \PDO::PARAM_INT)
				)
			);
		} else {
			$queryBuilder->andWhere(
				$queryBuilder->expr()->eq('uid', $queryBuilder->createNamedParameter($this->getUid(), \PDO::PARAM_INT))
			);
		}

		$olrow = $queryBuilder->setMaxResults(1)->executeQuery()->fetchAssociative();
		if (empty($olrow) || !$this->getEnableFieldResult($olrow)) {
			$olrow = [];
		}

		return $olrow;
	}
}
