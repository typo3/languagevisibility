<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace TYPO3\Languagevisibility\Element;

use Doctrine\DBAL\Driver\Exception;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Context\Exception\AspectNotFoundException;
use TYPO3\CMS\Core\Site\Entity\SiteLanguage;
use TYPO3\CMS\Core\Site\SiteFinder;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\MathUtility;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use TYPO3\Languagevisibility\Exception\InvalidRowException;
use TYPO3\Languagevisibility\Repository\VisibilityFlagRepository;

/**
 * Abstract basis class for all elements (elements are any translatable records in the system)
 */
abstract class Element {
	/**
	 * @var string
	 */
	protected string $table = '';

	/**
	 * @var array
	 */
	protected array $row = [];

	/**
	 * This array holds the global visibility settings
	 *
	 * @var array
	 */
	protected array $globalVisibilitySettings = [];

	/**
	 * @throws InvalidRowException|Exception
	 */
	public function __construct(array $row, string $tablename = '') {
		$this->table = $tablename;
		if (!$this->isRowOriginal($row)) {
			throw new InvalidRowException('Invalid Row Given');
		}
		$this->row = $row;

		$this->globalVisibilitySettings = $this->fetchGlobalVisibilitySettings();
		$this->initialisations();
	}

	/**
	 * Sets the table name
	 *
	 * @param string $table
	 * @return void
	 */
	public function setTable(string $table): void {
		$this->table = $table;
	}

	/**
	 * Gets the table name
	 *
	 * @return string
	 */
	public function getTable(): string {
		return $this->table;
	}

	/**
	 * Method to determine that an Element will not be instantiated with
	 * data of an overlay.
	 *
	 * @param array $row
	 * @return bool
	 */
	protected function isRowOriginal(array $row): bool {
		return !isset($row[$GLOBALS['TCA'][$this->table]['ctrl']['transOrigPointerField']]) ||
			$row[$GLOBALS['TCA'][$this->table]['ctrl']['transOrigPointerField']] === 0 ||
			(is_array($row[$GLOBALS['TCA'][$this->table]['ctrl']['transOrigPointerField']]) &&
			count($row[$GLOBALS['TCA'][$this->table]['ctrl']['transOrigPointerField']]) <= 0);
	}

	/**
	 * possibility to add inits in subclasses
	 *
	 * @return void
	 **/
	protected function initialisations(): void {
	}

	/**
	 * Returns the Uid of the Element
	 *
	 * @return int
	 */
	public function getUid(): int {
		return (int) $this->row['uid'];
	}

	/**
	 * Returns the pid of the Element
	 *
	 * @return int
	 */
	public function getPid(): int {
		return (int) $this->row['pid'];
	}

	/**
	 * Return the content of the title field
	 *
	 * @return string
	 */
	public function getTitle(): string {
		return (string) $this->row['title'];
	}

	/**
	 * Returns the uid of the original element. This method will only return
	 * a non-zero value if the element is an overlay;
	 *
	 * @return int
	 */
	public function getOrigElementUid(): int {
		if (isset($this->row[$GLOBALS['TCA'][$this->table]['ctrl']['transOrigPointerField']])) {
			return (int) $this->row[$GLOBALS['TCA'][$this->table]['ctrl']['transOrigPointerField']];
		}

		return 0;
	}

	/**
	 * Returns a description of the element.
	 *
	 * @return string
	 */
	public function getInformativeDescription(): string {
		if ($this->isMonolithicTranslated()) {
			return LocalizationUtility::translate(
				'backend.contentElement.notDefault',
				'languagevisibility'
			);
		}

		if ($this->isLanguageSetToAll()) {
			return LocalizationUtility::translate(
				'backend.contentElement.languageSetToAll',
				'languagevisibility'
			);
		}

		if ($this->isLanguageSetToDefault()) {
			return LocalizationUtility::translate(
				'backend.contentElement.normal',
				'languagevisibility'
			);
		}

		return LocalizationUtility::translate(
			'backend.contentElement.translated',
			'languagevisibility'
		);
	}

	/**
	 * This method is only need to display the visibility setting in the backend.
	 *
	 * @param int $languageUid
	 * @return string
	 */
	public function getVisibilitySettingForLanguage(int $languageUid): string {
		return $this->globalVisibilitySettings[$languageUid] ?? '';
	}

	/**
	 * fetches and sets the global / default language visibility settings for the element
	 *
	 * @return array
	 * @throws Exception|\Doctrine\DBAL\Exception
	 */
	protected function fetchGlobalVisibilitySettings(): array {
		$globalVisibilitySettings = [];
		$siteFinder = GeneralUtility::makeInstance(SiteFinder::class);

		$visibilityFlagRepository = GeneralUtility::makeInstance(VisibilityFlagRepository::class);

		if ($this->table === 'pages') {
			$pid = $this->getUid();
		} else {
			$pid = $this->getPid();
		}

		try {
			$rootLine = BackendUtility::BEgetRootLine($pid);
			$site = $siteFinder->getSiteByPageId($pid, $rootLine);
		} catch (\Exception $e) {
			return [];
		}

		$uid = $this->getUid();
		$availablesLanguages = $site->getAllLanguages();
		foreach ($availablesLanguages as $language) {
			$languageId = $language->getLanguageId();
			$flagRow = $visibilityFlagRepository->getVisibilityFlag($this->table, $uid, $languageId);

			$flag = 'active';
			if ($flagRow) {
				$flag = $flagRow['flag'];
			}

			$globalVisibilitySettings[$languageId] = $flag;
		}

		return $globalVisibilitySettings;
	}

	/**
	 * Check if the element is set to the default language
	 *
	 * @return bool
	 */
	public function isLanguageSetToDefault(): bool {
		return (int) $this->row['sys_language_uid'] === 0;
	}

	/**
	 * Determines if the elements is a original or a overlay-element
	 *
	 * @return bool
	 */
	protected function isOrigElement(): bool {
		return !($this->getOrigElementUid() > 0);
	}

	/**
	 * Checks if the current record is set to language all (that is typically used to indicate that per default this element is visible in all languages)
	 *
	 * @return bool
	 */
	public function isLanguageSetToAll(): bool {
		return (int) $this->row['sys_language_uid'] === -1;
	}

	/**
	 * Determines whether the element is a translated original record ...
	 *
	 * @return bool
	 */
	public function isMonolithicTranslated(): bool {
		return (!$this->isLanguageSetToDefault()) && (!$this->isLanguageSetToAll()) && $this->isOrigElement();
	}

	/**
	 * Compare element-language and foreign language.
	 *
	 * @param SiteLanguage $language
	 * @return bool
	 */
	public function languageEquals(SiteLanguage $language): bool {
		return (int) $this->row['sys_language_uid'] === $language->getLanguageId();
	}

	/**
	 * Checks if this element has a translation, therefore several DB accesses are required
	 *
	 * @param int $languageid
	 * @return bool
	 */
	public function hasTranslation(int $languageid): bool {
		if (!is_numeric($languageid)) {
			return FALSE;
		}

		if ($languageid === 0) {
			return TRUE;
		}

		if ($this->_hasOverlayRecordForLanguage($languageid)) {
			return TRUE;
		}

		return FALSE;
	}

	/**
	 * This method can be used to determine if an overlay for a language exists.
	 *
	 * @param int $languageId
	 * @return bool
	 */
	protected function _hasOverlayRecordForLanguage(int $languageId): bool {
		$row = $this->getOverLayRecordForCertainLanguageImplementation($languageId);
		return isset($row['uid']) && (int) $row['uid'] !== 0;
	}

	/**
	 * Method to get a short description  of the elementType.
	 * An extending class should overwrite this method.
	 *
	 * @return string
	 */
	public function getElementDescription(): string {
		return 'TYPO3 Element';
	}

	/**
	 * Check the records enableColumns
	 *
	 * @param array $row
	 * @return bool
	 * @throws AspectNotFoundException
	 */
	protected function getEnableFieldResult(array $row): bool {
		$ctrl = $GLOBALS['TCA'][$this->table]['ctrl'];
		$enabled = TRUE;
		if (is_array($ctrl['enablecolumns'])) {
			if ($ctrl['enablecolumns']['disabled'] ?? FALSE) {
				$enabled = $row[$ctrl['enablecolumns']['disabled']] === 0;
			}

			if ($ctrl['enablecolumns']['starttime'] ?? FALSE) {
				$enabled &= $row[$ctrl['enablecolumns']['starttime']] <= $GLOBALS['EXEC_TIME'];
			}

			if ($ctrl['enablecolumns']['endtime'] ?? FALSE) {
				$endtime = (int) $row[$ctrl['enablecolumns']['endtime']];
				$enabled &= $endtime === 0 || $endtime > $GLOBALS['EXEC_TIME'];
			}

			if (
				isset($ctrl['enablecolumns']['fe_group'], $GLOBALS['TSFE'])
				&& $ctrl['enablecolumns']['fe_group']
				&& is_object($GLOBALS['TSFE'])
			) {
				$fe_group = $row[$ctrl['enablecolumns']['fe_group']];
				if ($fe_group) {
					$context = GeneralUtility::makeInstance(Context::class);
					$currentUserGroups = $context->getPropertyFromAspect('frontend.user', 'groupIds');

					$recordGroups = GeneralUtility::intExplode(',', $fe_group);
					$sharedGroups = array_intersect($recordGroups, $currentUserGroups);
					$enabled &= count($sharedGroups) > 0;
				}
			}
		}

		return $enabled;
	}

	/**
	 * Check if the element is new
	 *
	 * @return bool
	 */
	public function isNew(): bool {
		return !MathUtility::canBeInterpretedAsInteger($this->row['uid']);
	}

	/**
	 * Get the new uid as proper string
	 *
	 * @return string
	 */
	public function getNewUid(): string {
		return $this->row['uid'];
	}

	/**
	 * This method should provide the implementation to get the overlay of an element for a
	 * certain language. The result is cached be the method getOverLayRecordForCertainLanguage.
	 *
	 * @param int $languageId
	 * @return array
	 */
	abstract protected function getOverLayRecordForCertainLanguageImplementation(int $languageId): array;
}
