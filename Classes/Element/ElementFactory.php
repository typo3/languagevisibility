<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace TYPO3\Languagevisibility\Element;

use Doctrine\DBAL\Exception;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\Restriction\DeletedRestriction;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\Languagevisibility\Exception\TableNotSupportedException;
use TYPO3\Languagevisibility\Service\VisibilityService;

/**
 * Class ElementFactory
 */
class ElementFactory {
	/**
	 * @var VisibilityService
	 */
	protected VisibilityService $visibilityService;

	/**
	 * @var ConnectionPool
	 */
	protected ConnectionPool $connectionPool;

	public function __construct(
		VisibilityService $visibilityService,
		ConnectionPool $connectionPool
	) {
		$this->visibilityService = $visibilityService;
		$this->connectionPool = $connectionPool;
	}

	/**
	 * Returns ready initialised "element" object. Depending on the element the correct element class is used. (e.g. page/content/fce)
	 *
	 * @param string $table table
	 * @param array $row identifier
	 * @return Element
	 * @throws TableNotSupportedException
	 * @throws Exception
	 * @throws \Exception
	 */
	public function getElementForTable(string $table, array $row): Element {
		if (!$this->visibilityService->isSupportedTable($table)) {
			throw new TableNotSupportedException(
				$table . ' not supported. See the README.md from EXT:languagevisibility.',
				1195039394
			);
		}

		if (
			$row[$GLOBALS['TCA'][$table]['ctrl']['languageField']] &&
			$row[$GLOBALS['TCA'][$table]['ctrl']['transOrigPointerField']] > 0
		) {
			$queryBuilder = $this->connectionPool->getQueryBuilderForTable($table);
			$queryBuilder->getRestrictions()
				->removeAll()
				->add(GeneralUtility::makeInstance(DeletedRestriction::class));
			$fetchedRow = $queryBuilder
				->select('*')
				->from($table)
				->where(
					$queryBuilder->expr()->eq(
						'uid',
						$queryBuilder->createNamedParameter(
							$row[$GLOBALS['TCA'][$table]['ctrl']['transOrigPointerField']],
							\PDO::PARAM_INT
						)
					)
				)
				->setMaxResults(1)
				->executeQuery()->fetchAssociative();
			if ($fetchedRow === FALSE) {
				throw new \Exception('Default language record not found!');
			}

			$row = $fetchedRow;
		}

		if ($table === 'tt_content') {
			$element = GeneralUtility::makeInstance(ContentElement::class, $row, $table);
		} else {
			$element = GeneralUtility::makeInstance(RecordElement::class, $row, $table);
		}

		$element->setTable($table);
		return $element;
	}
}
