<?php

use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

$tempColumnsElements = [
	'tx_languagevisibility_visibility' => [
		'exclude' => 1,
		'l10n_display' => 'hideDiff',
		'label' => 'LLL:EXT:languagevisibility/Resources/Private/Language/locallang_db.xlf:pages.tx_languagevisibility_visibility',
		'config' => [
			'type' => 'user',
			'renderType' => 'languageVisibility'
		]
	]
];

ExtensionManagementUtility::addTCAcolumns('tt_content', $tempColumnsElements);

$GLOBALS['TCA']['tt_content']['palettes']['general']['showitem'] = str_replace(
	'sys_language_uid;LLL:EXT:cms/locallang_ttc.xml:sys_language_uid_formlabel',
	'',
	$GLOBALS['TCA']['tt_content']['palettes']['general']['showitem']
);

// Add description to a hidden key where languagevisibility is present
// Do not move this out of here, because ext_tables is not the place to alter TCA directly
foreach ($GLOBALS['TCA'] as &$globalTcaColumn) {
	if (isset($globalTcaColumn['columns']['tx_languagevisibility_visibility'])) {
		$hiddenKey = $globalTcaColumn['ctrl']['enablecolumns']['disabled'];
		$globalTcaColumn['columns'][$hiddenKey]['description'] = 'LLL:EXT:languagevisibility/Resources/Private/Language/locallang_db.xlf:hidden_key_desc';
	}
}
