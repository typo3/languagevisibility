<?php

use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

$tempColumnsPages = [
	'tx_languagevisibility_visibility' => [
		'exclude' => 1,
		'label' => 'LLL:EXT:languagevisibility/Resources/Private/Language/locallang_db.xlf:pages.tx_languagevisibility_visibility',
		'config' => [
			'type' => 'user',
			'renderType' => 'languageVisibility'
		]
	],
];
ExtensionManagementUtility::addTCAcolumns('pages', $tempColumnsPages);

// l18n_cfg should be reset to default values (no sense in combination with languagevisibility)
$GLOBALS['TCA']['pages']['columns']['l18n_cfg']['config']['readOnly'] = TRUE;
