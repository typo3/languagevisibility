<?php

return [
	'ctrl' => [
		'label' => '',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'hideTable' => TRUE,
		'security' => [
			'ignorePageTypeRestriction' => TRUE
		]
	],
	'columns' => [
		'record_table' => [
			'config' => [
				'type' => 'input'
			]
		],
		'record_uid' => [
			'config' => [
				'type' => 'input'
			]
		],
		'record_language_uid' => [
			'config' => [
				'type' => 'number'
			]
		],
		'flag' => [
			'config' => [
				'type' => 'input'
			]
		],
	],
];
