<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use TYPO3\CMS\Core\Configuration\Event\AfterTcaCompilationEvent;
use TYPO3\CMS\Frontend\Event\FilterMenuItemsEvent;
use TYPO3\Languagevisibility\Element\ContentElement;
use TYPO3\Languagevisibility\Element\Element;
use TYPO3\Languagevisibility\Element\ElementFactory;
use TYPO3\Languagevisibility\Element\RecordElement;
use TYPO3\Languagevisibility\EventListeners\AfterTcaCompilationEventListener;
use TYPO3\Languagevisibility\EventListeners\FilterMenuItemsEventListener;
use TYPO3\Languagevisibility\Service\BackendServices;
use TYPO3\Languagevisibility\Service\FrontendServices;
use TYPO3\Languagevisibility\Xclass\Gridelements\Gridelements;
use TYPO3\Languagevisibility\Xclass\Mask\InlineHelper;
use TYPO3\Languagevisibility\Xclass\PageRepository;
use function Symfony\Component\DependencyInjection\Loader\Configurator\service;

// Define the required dependencies excl. gridelements/mask if not installed
return static function (ContainerConfigurator $containerConfigurator): void {
	$services = $containerConfigurator->services();
	$services->defaults()
		->private()
		->autowire()
		->autoconfigure();

	$services->load('TYPO3\\Languagevisibility\\', __DIR__ . '/../Classes/')->exclude([
		__DIR__ . '/../Classes/Xclass/Gridelements',
		__DIR__ . '/../Classes/Xclass/Mask',
	]);

	$services->set(Element::class)->autowire(FALSE)->autoconfigure(FALSE);
	$services->set(ContentElement::class)->autowire(FALSE)->autoconfigure(FALSE);
	$services->set(RecordElement::class)->autowire(FALSE)->autoconfigure(FALSE);
	$services->set(FrontendServices::class)->public();
	$services->set(BackendServices::class)->public();
	$services->set(ElementFactory::class)->public();
	$services->set(
		\TYPO3\CMS\Core\Domain\Repository\PageRepository::class,
		service(PageRepository::class)
	);
	$services->set(FilterMenuItemsEventListener::class)
		->tag(
			'event.listener',
			[
				'identifier' => 'filterMenuItemsEventListener',
				'event' => FilterMenuItemsEvent::class
			]
		);
	$services->set(AfterTcaCompilationEventListener::class)
		->tag(
			'event.listener',
			[
				'identifier' => 'afterTcaCompilationEventListener',
				'event' => AfterTcaCompilationEvent::class
			]
		);

	// removed ExtensionManagementUtility::isLoaded() calls, because these fail randomly,
	// and only work clearly after full cache_clear for a while
	if (@file_exists(__DIR__ . '/../../mask')) {
		$services->set(InlineHelper::class)
			->autowire(FALSE)->autoconfigure(FALSE);
	}
};
