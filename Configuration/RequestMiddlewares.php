<?php

use TYPO3\Languagevisibility\Middleware\LanguageVisibilityResolver;

return [
	'frontend' => [
		'typo3/languagevisibility/languagevisibility-resolver' => [
			'target' => LanguageVisibilityResolver::class,
			'before' => [
				'typo3/cms-frontend/tsfe'
			],
			'after' => [
				'typo3/cms-frontend/page-resolver'
			]
		]
	]
];
